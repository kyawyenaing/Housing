<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    use Notifiable;


    protected $table="departments";
    public function ministry() {
    	return $this->belongsto('App\Ministry');
    }
}
