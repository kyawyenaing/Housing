<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StaffRequest;
use App\Staff;
use App\Department;
use App\Allocate;
use Validator;
use Datatables;
class StaffController extends Controller
{

    public function index() {
        return view('staff.index', compact('staffs'));
    }

    public function create() {
        return view('staff.create');

    }

    public function store(StaffRequest $request) {
        $ministry = Department::find($request->department_id);
        $ministry_id = $ministry->ministry_id;
        $staff = new Staff;
        $staff->name = $request->name;
        $staff->name_en = $request->name_en;
        $staff->position_id = $request->position_id;
        $staff->department_id = $request->department_id;
        $staff->ministry_id = $ministry_id;
        $staff->address = $request->address;
        $staff->save();
        return redirect('/staff')->with ('success','ဝန်ထမ်းအသစ်တစ်ယောက် ထည့်သွင်းပြီးပါပြီ');
    }

    public function edit($id) {
        $staff = Staff::find($id);
        return view('staff.edit' ,compact('staff'));

    }

    public function update(StaffRequest $request, $id) {
        $ministry = Department::find($request->department_id);
        $ministry_id = $ministry->ministry_id;
        $staff = Staff::find($id);
        $staff->name = $request->name;
        $staff->name_en = $request->name_en;
        $staff->department_id = $request->department_id;
        $staff->ministry_id = $ministry_id;
        $staff->position_id = $request->position_id;
        $staff->address = $request->address;
        $staff->save();
        $allocate = Allocate::where('staff_id',$staff->id)->get();
        foreach($allocate as $result) {
            $result->ministry_id = $staff->ministry_id;
            $result->save();
        }
        return redirect('/staff')->with ('success','ဝန်ထမ်း၏အချက်အလက်များကို ပြင်ဆင်ပြီးပါပြီ');
    }

    public function destroy($id) {
        $staff = Staff::find($id);
        $staff->forceDelete();
        return redirect('/staff')->with('success','Staff has been deleted successfully');
    }

    public function datatable() {
        $result = Staff::with('position')->with('department')->with('ministry');
        $datatables = Datatables::eloquent($result)
                ->addIndexColumn()
                ->addColumn('action',function($results){
                $url = url('');
                $token = csrf_token();
      return '
            <div class="action-buttons">
              <a href="'.$url.'/staff/'.$results->id.'/edit" class="green">
                <i class="ace-icon fa fa-pencil-square-o  bigger-130"></i>
              </a>
              <form action="'.$url.'/staff/'.$results->id.'" method="post" class="inline">
                  <input type="hidden" name="_method" value="DELETE">
                  <input type="hidden" name="_token" value="'.$token.'">
                  <a data-id="" class="red" data-toggle="modal" data-target="#confirmDelete">
                    <i class="ace-icon fa fa-trash-o bigger-130"></i>
                </a>
              </form>
            </div>
        ';
      });
        return $datatables->make(true);
    }

    public function allocate() {

        $results = Staff::$result = Staff::with('position')->with('department')->with('ministry');
        $datatables = Datatables::eloquent($result)
        ->where('allocate',2)->get();
        $datatables =  app('datatables')->of($results)
                ->addColumn('action',function($results){
                $url = url('');
                $token = csrf_token();
      return '
            <div class="action-buttons">
              <form action="'.$url.'/allocate/'.$results->id.'" method="post" class="inline">
                  <input type="hidden" name="_token" value="'.$token.'">
                  <input type="hidden" name="staff_id" value="'.$results->id.'">
                  <a data-id="" class="green" data-toggle="modal" data-target="#confirmAllocate">
                    <i class="ace-icon fa fa-pencil-square-o  bigger-130"></i>
                </a>
              </form>
            </div>
        ';
      });
        return $datatables->make(true);
    }
}
