<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\QuarterRequest;
use App\Quarter;
use Validator;
use Datatables;
class QuarterController extends Controller
{
   
    public function index() {
        return view('married_housings.quarter.index');
    }

    public function create() {
        return view('married_housings.quarter.create');
    }

    public function store(QuarterRequest $request) {
        $quarter = new Quarter;
        $quarter->name = $request->input('name');
        $quarter->short_name = $request->input('short_name');
        $quarter->description = $request->input('description');
        $quarter->save();
        return redirect('/quarter')->with('success','တည်နေရာ ရပ်ကွက် အသစ်တစ်ခု ထည့်သွင်းပြီးပါပြီ');
    }

    public function edit($id) {
        $quarter = Quarter::find($id); 
        return view('married_housings.quarter.edit', compact('quarter'));
    }

    public function update(Request $request, $id) {
        $quarter = Quarter::find($id); 
        $quarter->name = $request->input('name');
        $quarter->short_name = $request->input('short_name');
        $quarter->description = $request->input('description');
        $quarter->save();
        return redirect('/quarter')->with('success','တည်နေရာ ရပ်ကွက်၏ အချက်အလက်များကို ပြင်ဆင်ပြီးပါပြီ');
    }

    public function destroy($id) {
        $quarter = Quarter::find($id); 
        $quarter->forceDelete();
        return redirect('/quarter')->with('success', 'Quarter has been delete successfully');
    }
    
    public function datatable() {
        $results = Quarter::query();
        $datatables = Datatables::eloquent($results)
                ->addIndexColumn()
                ->addColumn('action',function($results){
                $url = url('');
                $token = csrf_token();
      return '
        <div class="action-buttons">
          <a href="'.$url.'/quarter/'.$results->id.'/edit" class="green">
            <i class="ace-icon fa fa-pencil-square-o  bigger-130"></i>
          </a>
          <form action="'.$url.'/quarter/'.$results->id.'" method="post" class="inline">
              <input type="hidden" name="_method" value="DELETE">
              <input type="hidden" name="_token" value="'.$token.'">
              <a data-id="" class="red" data-toggle="modal" data-target="#confirmDelete">
                <i class="ace-icon fa fa-trash-o bigger-130"></i>
            </a>
          </form>
        </div>
        ';
      });
        return $datatables->make(true);
    }
}
