<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
class UserController extends Controller
{
	
	public function index()
	{
		$users = User::get();
    	//return($users);
		return view('Admin.index',compact('users'));
	}
	public function create()
	{
		
		return view('Admin.create');
	}
	public function store(Request $request)
	{
		$this->validate($request, [
			'name' => 'required',
			'email' => 'required|email|unique:users',
			'password' => 'required|min:6|confirmed',
		]);
		$user = new User;
		$user->name = $request->input('name');
		$user->email = $request->input('email');
		$user->password = bcrypt($request->input('password'));
		$user->save();
		return redirect('/user')->with ('success','Admin has been Created successfully');
	}
	public function show($id)
	{
	    //
	}
	public function edit(User $user)
	{
		return view('Admin.edit',compact('user'));
	}

	public function update(Request $request,User $user)
	{
		if ($request->has('password')) {
			$this->validate($request, [
				'name' => 'required',
				'email' => 'required|email|unique:users,email,' . $user->id,
				'password' => 'required|min:6|confirmed',
			]);
		}
		else {
			$this->validate($request, [
				'name' => 'required',
				'email' => 'required|email|unique:users,email,' . $user->id,
			]);
		}            
		$user->name = $request->input('name');
		$user->email = $request->input('email');
		if ($request->has('password')) {
			$user->password = bcrypt($request->input('password'));
		}
		$user->save();
		return redirect('/user')->with ('success','Admin has been saved successfully');
	}
	public function destroy(User $user) {
		$user->forceDelete();
		return redirect('/user')->with('success','Admin has been deleted successfully');
	}

	public function datatable() {
	    $results = User::select('*');
	    $datatables =  app('datatables')->of($results)
	            ->addColumn('action',function($results){
	            $url = url('');
	            $token = csrf_token();
	  return '
	    <div class="action-buttons">
	      <a href="'.$url.'/department/'.$results->id.'/edit" class="green">
	        <i class="ace-icon fa fa-pencil-square-o  bigger-130"></i>
	      </a>
	      <form action="'.$url.'/department/'.$results->id.'" method="post" class="inline">
	          <input type="hidden" name="_method" value="DELETE">
	          <input type="hidden" name="_token" value="'.$token.'">
	          <a data-id="" class="red" data-toggle="modal" data-target="#confirmDelete">
	            <i class="ace-icon fa fa-trash-o bigger-130"></i>
	        </a>
	      </form>
	    </div>
	    ';
	  });
	return $datatables->make(true);
	}

}