<?php

namespace App\Http\Controllers;

use App\Http\Requests\MinistryRequest;
use App\Ministry;
use Datatables;
class MinistryController extends Controller
{
  
    public function index() {
        return view('ministry.index');
    }

    public function create() {
        return view('ministry.create');
    }

    public function store(MinistryRequest $request) {  
        $ministry = new Ministry;
        $ministry->name = $request->input('name');
        $ministry->name_en = $request->input('name_en');
        $ministry->description = $request->input('description');
        $ministry->save();
        return redirect('/ministry')->with ('success','ဝန်ကြီးဌာနသစ်တစ်ခု ထည့်သွင်းပြီးပါပြီ');
    }

    public function edit($id) {
        $ministry = Ministry::find($id); 
        return view('ministry.edit' ,compact('ministry'));
    }

    public function update(MinistryRequest $request, $id) {
        $ministry = Ministry::find($id);   
        $ministry->name = $request->input('name');
        $ministry->name_en = $request->input('name_en');
        $ministry->description = $request->input('description');
        $ministry->save();
        return redirect('/ministry')->with ('success','ဝန်ကြီးဌာန၏ အချက်အလက်များကို ပြင်ဆင်ပြီးပါပြီ');
    }
  
    public function destroy($id) {
        $ministry = Ministry::find($id); 
        $ministry->forceDelete();
        return redirect('/ministry')->with('success','Ministry has been deleted successfully');
    }

    public function datatable() {
        $results = Ministry::query()->orderBy('id','asc');
        $datatables = Datatables::eloquent($results)
                ->addIndexColumn()
                ->addColumn('action',function($results){
                $url = url('');
                $token = csrf_token();
      return '
        <div class="action-buttons">
          <a href="'.$url.'/ministry/'.$results->id.'/edit" class="green">
            <i class="ace-icon fa fa-pencil-square-o  bigger-130"></i>
          </a>
          <form action="'.$url.'/ministry/'.$results->id.'" method="post" class="inline">
              <input type="hidden" name="_method" value="DELETE">
              <input type="hidden" name="_token" value="'.$token.'">
              <a data-id="" class="red" data-toggle="modal" data-target="#confirmDelete">
                <i class="ace-icon fa fa-trash-o bigger-130"></i>
            </a>
          </form>
        </div>
        ';
      });
        return $datatables->make(true);
    }
}
