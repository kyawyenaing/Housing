<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PositionRequest;
use App\Position;
use Validator;
use Datatables;
class PositionController extends Controller
{

    public function index() {
        return view('position.index');
    }
    
    public function create() {
        return view('position.create');
    }

    public function store(PositionRequest $request) {  
        $position = new Position;
        $position->name = $request->input('name');
        $position->name_en = $request->input('name_en');
        $position->description = $request->input('description');
        $position->save();
        
        return redirect('/position')->with ('success','ရာထူးအသစ်တစ်ခု ထည့်သွင်းပြီးပါပြီ');
    }

    public function edit($id) {
        $position = Position::find($id);
        return view('position.edit' ,compact('position'));
    }

    public function update(PositionRequest $request, $id) {
        $position = Position::find($id);
        $position->name = $request->input('name');
        $position->name_en = $request->input('name_en');
        $position->description = $request->input('description');
        $position->save();
        return redirect('/position')->with ('success','ရာထူး၏ အချက်အလက်များကို ပြင်ဆင်ပြီးပါပြီ');
    }

    public function destroy($id){
        $position = Position::find($id);
        $position->forceDelete();
        return redirect('/position')->with('success','Position has been deleted successfully');
    }

    public function datatable() {
        $results = Position::query();
        $datatables = Datatables::eloquent($results)
                ->addIndexColumn()
                ->addColumn('action',function($results){
                $url = url('');
                $token = csrf_token();
      return '
            <div class="action-buttons">
              <a href="'.$url.'/position/'.$results->id.'/edit" class="green">
                <i class="ace-icon fa fa-pencil-square-o  bigger-130"></i>
              </a>
              <form action="'.$url.'/position/'.$results->id.'" method="post" class="inline">
                  <input type="hidden" name="_method" value="DELETE">
                  <input type="hidden" name="_token" value="'.$token.'">
                  <a data-id="" class="red" data-toggle="modal" data-target="#confirmDelete">
                    <i class="ace-icon fa fa-trash-o bigger-130"></i>
                </a>
              </form>
            </div>
        ';
      });
        return $datatables->make(true);
    }
}
