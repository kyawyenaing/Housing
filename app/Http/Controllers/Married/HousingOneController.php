<?php

namespace App\Http\Controllers\Married;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\HousingOneRequest;
use App\HousingOne;
use App\Staff;
use App\LivingHousing;
use App\HousingCategory;
class HousingOneController extends Controller
{

    public function index() {
        $housing_category = HousingCategory::where('id',1)->first();
        return view('married_housings.housing_one.index',compact('housing_category'));
    }

    public function create() {
        $housing_category = HousingCategory::where('id',1)->first();
        return view('married_housings.housing_one.create', compact('housing_one','housing_category'));
    }

    public function store(HousingOneRequest $request) {
        $housing_one = new HousingOne;
        $housing_one->housing_category_id = 1;
        $housing_one->quarter_id = $request->quarter_id;
        $housing_one->building_id = $request->building_id;
        $housing_one->room_no = $request->room_no;
        if($request->description) {
            $housing_one->description = $request->description;
        }
        $housing_one->save();
        return redirect('married/housing_one')->with ('success','အိမ်ခန်းအသစ်တစ်ခု ထည့်သွင်းပြီးပါပြီ');
    }

    public function show($id) {
        $housing_one = HousingOne::find($id);
        return view('married_housings.housing_one.show',compact('housing_one'));
    }

    public function edit($id) { 
        $housing_one = HousingOne::find($id); 
        return view('married_housings.housing_one.edit' ,compact('housing_one'));
    }

    public function update(HousingOneRequest $request, $id) {
        $housing_one = HousingOne::find($id); 
        $housing_one->quarter_id = $request->quarter_id;
        $housing_one->building_id = $request->building_id;
        $housing_one->room_no = $request->room_no;
        $housing_one->description = $request->description;
        if($request->staff_id != $housing_one->staff_id) {
            $old_staff = Staff::find($housing_one->staff_id);
            $old_staff->allocate = 1;
            $old_staff->save();
            $new_staff = Staff::find($request->staff_id);
            $new_staff->allocate = 2;
            $new_staff->save();
            $housing_one->staff_id = $new_staff->id;
            $housing_one->ministry_id = $new_staff->ministry_id;
        }
        if($request->living_date != $housing_one->living_date) {
            $housing_one->living_date = date("Y-m-d", strtotime( $request->living_date));
        }
        $housing_one->save();
        return redirect('married/housing_one')->with ('success','အိမ်ခန်း၏အချက်အလက်များကို ပြင်ဆင်ပြီးပါပြီ');
    }

    public function destroy($id) {
        $housing_one = HousingOne::find($id); 
        $housing_one->forceDelete();
        return redirect('married/housing_one')->with('success','Deleted');
    }

    public function datatable() {
        $results = HousingOne::select([
                    'housing_ones.id',
                    'housing_ones.room_no',
                    'housing_ones.description',
                    'housing_ones.allocate',
                    'buildings.name AS building_no',
                    'quarters.name AS quarter_name'
                    ])
        ->leftJoin('buildings','buildings.id','=','housing_ones.building_id')
        ->leftJoin('quarters','quarters.id','=','housing_ones.quarter_id');
        $datatables =  app('datatables')->of($results)
            ->addColumn('action',function($results){
            $url = url('');
            $token = csrf_token();
            if($results->allocate ==1 && $results->allocate !=2) {
                return '
                <div class="action-buttons">
                  <a href="'.$url.'/married/housing_one/'.$results->id.'/edit" class="green">
                    <i class="ace-icon fa fa-pencil-square-o  bigger-130"></i>
                  </a> |
                  <form action="'.$url.'/married/housing_one/'.$results->id.'" method="post" class="inline">
                      <input type="hidden" name="_method" value="DELETE">
                      <input type="hidden" name="_token" value="'.$token.'">
                      <a data-id="" class="red" data-toggle="modal" data-target="#confirmDelete">
                        <i class="ace-icon fa fa-trash-o bigger-130"></i>
                    </a>
                  </form> |
                  <form action="'.$url.'/married/housing_one/'.$results->id.'/init_allocate" method="get" class="inline">
                      <a data-id="" class="red" data-toggle="modal" data-target="#confirmAllocate">
                       <b class="text-primary hov">နေရာချပေးရန်</b>
                    </a>
                  </form> 
                </div>
                ';
            } else {
                return '
                <div class="action-buttons">
                  <a href="'.$url.'/married/housing_one/'.$results->id.'/edit" class="green">
                    <i class="ace-icon fa fa-pencil-square-o  bigger-130"></i>
                  </a> |
                  <form action="'.$url.'/married/housing_one/'.$results->id.'" method="post" class="inline">
                      <input type="hidden" name="_method" value="DELETE">
                      <input type="hidden" name="_token" value="'.$token.'">
                      <a data-id="" class="red" data-toggle="modal" data-target="#confirmDelete">
                        <i class="ace-icon fa fa-trash-o bigger-130"></i>
                    </a>
                  </form> |
                    <b class="text-danger">နေရာချပေးပြီး</b>
                </div>
                ';
            }
        });
      // 
        if ($datatables->request->get('quarter')) {
            if($datatables->request->get('allocate')) {
                $datatables->where('housing_ones.quarter_id', '=', $datatables->request->get('quarter'))->where('housing_ones.allocate','=',$datatables->request->get('allocate'));
            } else {
                $datatables->where('housing_ones.quarter_id', '=', $datatables->request->get('quarter'));
            }
        }
        if($datatables->request->get('allocate')) {
            if ($datatables->request->get('quarter')) {
            $datatables->where('housing_ones.allocate','=',$datatables->request->get('allocate'))->where('housing_ones.quarter_id', '=', $datatables->request->get('quarter'));
            } 
            else {
                $datatables->where('housing_ones.allocate','=',$datatables->request->get('allocate'));
            }
        }
        return $datatables->make(true);
    }

    public function initAllocate(Request $request,$id) {
        $housing_one = HousingOne::find($id);
        return view('married_housings.housing_one.allocate',compact('housing_one','staffs'));
    }

    public function confirmAllocate(Request $request) {
        $allocate = HousingOne::find($request->id);
        $living_house = new LivingHousing;
        $living_house->staff_id = $request->staff_id;
        $living_house->housing_one_id = $request->housing_one_id;
        $living_house->living_date = date("Y-m-d", strtotime( $request->living_date));
        if($request->description) {
            $living_house->description = $request->description;
        }
        $living_house->save();
        $staff = Staff::find($request->staff_id);
        $staff->allocate = 2;
        $staff->save();
        $allocate->allocate = 2;
        $allocate->staff_id = $request->staff_id;
        $allocate->living_date = date("Y-m-d", strtotime( $request->living_date));
        $allocate->description = $request->description;
        $allocate->ministry_id = $staff->ministry_id;
        $allocate->save();
        return redirect('/married/housing_one')->with('success','ဝန်ထမ်းကို နေရာချထားပေးပြီးပါပြီ');  ;
    }
    public function living_housing() {
        $housing_category = HousingCategory::where('id',1)->first();
        $living_housings = HousingOne::where('allocate',2)->paginate(100);
        return view('married_housings.living_housing.list',compact('housing_category','living_housings'));
    }

    public function living_one($id) {
        $housing_category = HousingCategory::where('id',1)->first();
        $living_housings = HousingOne::where('quarter_id',$id)->where('allocate',2)->paginate(100);
        return view('married_housings.living_housing.list',compact('housing_category','living_housings'));
    }
}
