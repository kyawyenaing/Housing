<?php

namespace App\Http\Controllers\Married;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Allocate;
use App\Rank;
use App\Position;
use App\Department;
use App\Ministry;
use App\Quarter;
use App\HousingCategory;
use Datatables;
class AllocatedQuarterController extends Controller
{

    public function index() {
        return view('married_housings.allocated.quarters');
    }
    public function datatable() {
        $results = Allocate::with('staff')->with('ministry')->with('building')->where('status',1);
        $datatables = Datatables::eloquent($results)
                ->addIndexColumn()
                
                ->addColumn('position_name',function($results) {
                  $position = Position::find($results->staff->position_id);
                  return $position->name;

                })
                ->addColumn('department_name',function($results) {
                  $departmen = Department::find($results->staff->department_id);
                  return $departmen->name;
                });
        if ($datatables->request->get('quarter')) {
            $quarter_id = Quarter::where('name',$datatables->request->get('quarter'))->first();
            if($datatables->request->get('category')) {
              $datatables->where('quarter_id', '=',$quarter_id->id)->where('housing_category_id','=',$datatables->request->get('category'));
            }else {
              $datatables->where('quarter_id', '=',$quarter_id->id);
            }
          }
        if($datatables->request->get('category')) {
          if($datatables->request->get('quarter')) {
            $quarter_id = Quarter::where('name',$datatables->request->get('quarter'))->first();
            $datatables->where('housing_category_id','=',$datatables->request->get('category'))->where('quarter_id', '=',$quarter_id->id);
          }else {
            $datatables->where('housing_category_id','=',$datatables->request->get('category'));
          }
        }
        return $datatables->make(true);
    }
}
