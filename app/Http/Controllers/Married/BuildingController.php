<?php

namespace App\Http\Controllers\Married;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\BuildingRequest;
use App\Building;
use App\Allocate;
use Datatables;
class BuildingController extends Controller
{
    public function index() {
        return view('married_housings.building.index');
    }

    public function create() {
        return view('married_housings.building.create');
    }

    public function store(BuildingRequest $request) {
        $building = new Building;
        $building->building_no = $request->building_no;
        $building->housing_category_id = $request->housing_category_id;
        $building->quarter_id = $request->quarter_id;
        $building->description = $request->description;
        $building->save();

        return redirect('married/buildings')->with('success','အဆောက်အဦးအသစ်တစ်ခု ထည့်သွင်းပြီးပါပြီ');
    }

    public function edit($id) {
        $building = building::find($id); 
        return view('married_housings.building.edit', compact('building'));
    }

    public function update(BuildingRequest $request, $id) {
        $building = Building::find($id); 
        $building->building_no = $request->building_no;
        $building->housing_category_id = $request->housing_category_id;
        $building->quarter_id = $request->quarter_id;
        $building->description = $request->description;
        $building->save();
        $allocate = Allocate::where('building_id',$building->id)->get();
        foreach($allocate as $result) {
            $result->housing_category_id = $building->housing_category_id;
            $result->quarter_id = $building->quarter_id;
            $result->save();
        }
        return redirect('/married/buildings')->with('success','အဆောက်အဦး၏ အချက်အလက်များကို ပြင်ဆင်ပြီးပါပြီ');
    }

    public function destroy($id) {
        $building = Building::find($id); 
        $building->forceDelete();
        return redirect('married/buildings')->with('success', 'building has been delete successfully');
    }
    
    public function datatable() {
        $results = Building::with('housing_category')->with('quarter');
        $datatables = Datatables::eloquent($results)
                ->addIndexColumn()
                ->addColumn('action',function($results){
                $url = url('');
                $token = csrf_token();
      return '
        <div class="action-buttons">
          <a href="'.$url.'/married/buildings/'.$results->id.'/edit" class="green">
            <i class="ace-icon fa fa-pencil-square-o  bigger-130"></i>
          </a>
          
          |
          <form action="'.$url.'/married/'.$results->id.'/allocate/init" method="get" class="inline">
            <a data-id="" class="red" data-toggle="modal" data-target="#confirmAllocate">
               <b class="text-primary hov">နေရာချပေးရန်</b>
            </a>
          </form> 
        </div>
        ';
      });
      // 
      if ($datatables->request->get('category')) {
            if($datatables->request->get('quarter')) {
                $datatables->where('quarter_id', '=', $datatables->request->get('category'))->with('category')->with('quarter');
            } else {
                $datatables->where('housing_category_id', '=', $datatables->request->get('category'))->with('category')->with('quarter');
            }
        }
        if($datatables->request->get('quarter')) {
            if ($datatables->request->get('category')) {
            $datatables->where('quarter_id','=',$datatables->request->get('quarter'))->where('housing_category_id', '=', $datatables->request->get('category'))->with('category')->with('quarter');
            } 
            else {
                $datatables->where('quarter_id','=',$datatables->request->get('quarter'))->with('category')->with('quarter');
            }
        }
        return $datatables->make(true);
    }
}
