<?php

namespace App\Http\Controllers\Married;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Allocate;
use Datatables;
use App\Quarter;
use App\Building;
use App\Ministry;
class AllocatedMinistryController extends Controller
{

    public function index() {
      return view('married_housings.allocated.ministries');
    }

    public function datatable() {
      $results = Allocate::with('housing_category')->with('building')->with('ministry')->where('status',1)->groupBy('building_id')->groupBy('ministry_id')->groupBy('housing_category_id');
        $datatables = Datatables::eloquent($results)
                ->addIndexColumn()
                ->addColumn('quarter',function($results){
                  $quarter = Quarter::find($results->building->quarter_id);
                  return $quarter->name;
                })
                ->addColumn('room_no',function($results){
                  $room = Allocate::where('building_id',$results->building_id)->where('ministry_id',$results->ministry_id)->get();
                  $room_arr = array();
                  foreach($room as $res) {
                    $room_arr[]=$res->room_no;
                  }
                  return $room_arr;

                })
                ->addColumn('count_room',function($results) {
                  $total = Allocate::where('housing_category_id',$results->housing_category_id)->where('ministry_id',$results->ministry_id)->where('building_id',$results->building_id)->get();
                  return count($total);
                })
                ->addColumn('action',function($results){
                  $url = url('');
                  $token = csrf_token();
                  return '
                    <div class="action-buttons">
                      <form action="'.$url.'/married/'.$results->id.'/allocate/remark_create" method="get" class="inline">
                        <input type="hidden" name="_method" value="PUT">
                        <input type="hidden" name="_token" value="'.$token.'">
                        <input type="hidden" name="ministry_id" value=" '.$results->ministry_id.' ">
                        <input type="hidden" name="building_id" value=" '.$results->building_id.' ">
                        <textarea placeholder="မှတ်ချက်ရေးရန်" name="remark_by_ministry">
                        </textarea>
                        <button type="submit" class="btn btn-success btn-sm">
                          <i class="ace-icon fa fa-check bigger-110"></i>
                            မှတ်ချက်ရေးရန်
                        </button>
                      </form> 
                    </div>
                    ';
                });
      if ($datatables->request->get('ministry')) {
        $ministry_id = Ministry::where('name',$datatables->request->get('ministry'))->first();
          $datatables->where('ministry_id', '=',$ministry_id->id)->with('housing_category')->with('building')->with('ministry');
        }
        return $datatables->make(true);
    }
}
