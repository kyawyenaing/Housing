<?php

namespace App\Http\Controllers\Married;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\AllocateRequest;
use Validator;
use App\Building;
use App\Allocate;
use App\OldAllocated;
use App\Staff;
use App\Department;
use App\Ministry;
use Session;
class AllocateController extends Controller
{

    public function init($building_id) {
        $building = Building::find($building_id);
        return view('married_housings.allocate.init',compact('building'));
    }

    public function create(Request $request) {
        $building = Building::find($request->building_id);
        $staffs = Staff::where('ministry_id',$request->ministry_id)->where('allocate',2)->get();
        $ministry = Ministry::find($request->ministry_id);
        return view('married_housings.allocate.create',compact('building','staffs','ministry'));
    }

    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
                    'room_no.*' => 'required',
                    'staff_id.*' => 'required',
                ],
                ['staff_id.*.required'=>'နေရာချထားပေးမည့်ဝန်ထမ်း ကိုရွေးချယ်ပေးပါ',
                'room_no.*.required'=>'အခန်းအမှတ် ကိုရိုက်ထည့်ပေးပါ']);
        $building = Building::find($request->building_id);
        $staffs = Staff::where('ministry_id',$request->ministry_id)->get();
        if($validator->fails()) {
            return view('married_housings.allocate.create',compact('building','staffs'))->withErrors($validator);
        }
        $room_no = $request->room_no;
        $staff_id = $request->staff_id;
        foreach( $room_no as $key => $n ) {
            $allocate = new Allocate;
            $allocate->building_id = $building->id;
            $allocate->housing_category_id = $request->housing_category_id;
            $allocate->quarter_id = $request->quarter_id;
            $allocate->ministry_id = $request->ministry_id;
            $allocate->room_no =  $room_no[$key];
            $allocate->staff_id =  $staff_id[$key];
            $allocate->remark = $request->remark;
            $allocate->save();
            $staff = Staff::find($staff_id[$key]);
            $staff->allocate = 1;
            $staff->save();
        }
        return redirect('married/allocated-buildings')->with('success','Successfully allocated new room');
    }

    public function edit($id) {
        $allocate = Allocate::find($id);
        $staffs = Staff::all();
        return view('married_housings.allocate.edit',compact('allocate','staffs'));
    }

    public function reAllocate($id) {
        $allocate = Allocate::find($id);
        $staffs = Staff::where('allocate',2)->get();
        return view('married_housings.allocate.reallocate',compact('allocate','staffs'));
    }

    public function update(Request $request, $id) {
        $allocate = Allocate::find($id);
        if($request->staff_id == null )
            {
                $allocate->room_no =  $request->room_no;
                $allocate->remark = $request->remark;
                $allocate->save();
            } else {
            $allocate->room_no =  $request->room_no;
            $allocate->remark = $request->remark;
            $allocate->status = 1;
            $allocate->staff_id = $request->staff_id;
            $staff = Staff::find($request->staff_id);
            $staff->allocate = 1;
            $staff->save();
            $allocate->ministry_id = $staff->ministry_id;
            $allocate->save();
        }
        
        return redirect('married/allocated-buildings/')->with('success','Successfully Updated');
    }
    
    public function remark_create(Request $request,$id) {
        $allocate = Allocate::where('building_id',$request->building_id)->where('ministry_id',$request->ministry_id)->get();
        foreach($allocate as $result) {
            $result->remark_by_ministry = $request->remark_by_ministry;
            $result->save();
        }
        return redirect()->back()->with('success','Saved the Remark');
    }

    public function destroy($id) {
        //
    }
}
