<?php

namespace App\Http\Controllers\Married;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Allocate;
use App\Quarter;
use App\Staff;
use App\Department;
use App\Position;
use App\Rank;
use App\Building;
use App\Ministry;
use App\OldAllocated;
use Datatables;
class AllocatedBuildingController extends Controller
{
    public function index() {
    	return view('married_housings.allocated.buildings');
    }

    public function datatable() {
        $results = Allocate::with('staff')->with('building');
        $datatables = Datatables::eloquent($results)
                ->addIndexColumn()
                
                ->addColumn('staff_name',function($results){
                  if($results->staff_id) {
                  	return $results->staff->name;
                  } else {
                  	return "";
                  }
                })
                ->addColumn('position_name',function($results) {
                  if($results->staff_id) {
                  	$position = Position::find($results->staff->position_id);
                  	return $position->name;
                  } else {
                  	return "";
                  }

                })
                ->addColumn('department_name',function($results) {
                  if($results->staff_id) {
                  	$departmen = Department::find($results->staff->department_id);
                  	return $departmen->name;
                  } else {
                  	return "";
                  }
                })
                ->addColumn('ministry_name',function($results){
                	if($results->staff_id) {
                		$ministry = Ministry::find($results->staff->ministry_id);
                		return $ministry->name;
                	} else {
                		return "";
                	}
                })
                ->addColumn('action',function($results){
                  $url = url('');
                  $token = csrf_token();
                  if($results->status == 1){
                    return '<div class="action-buttons">
                            <a href="'.$url.'/married/allocate/'.$results->id.'/edit" class="green">
                            <i class="ace-icon fa fa-pencil-square-o bigger-130"></i>
                            </a>
                            |
                            <form action="'.$url.'/married/remove_staff/'.$results->id.'" method="post" class="inline">
                                <input type="hidden" name="_method" value="PUT">
                                <input type="hidden" name="_token" value="'.$token.'">
                                <input type="hidden" name="staff_id" value="'.$results->staff_id.'">
                                <a data-id="" class="red" data-toggle="modal" data-target="#removeStaff">
                                  <b class="text-danger">ဝန်ထမ်းကို အခန်းမှထုတ်မည်</b>
                              </a>
                            </form>
                          </div>';
                  } else {
                    return '<div class="action-buttons">
                            <a href="'.$url.'/married/allocate/'.$results->id.'/edit" class="green">
                            <i class="ace-icon fa fa-pencil-square-o bigger-130"></i>
                            </a>
                            |
                            <a href="'.$url.'/married/allocate/'.$results->id.'/reallocate" class="green">
                            <b class="text-success">နေရာချပေးရန်</b>
                            </a>
                          </div>';
                  }
                });
        if ($datatables->request->get('building')) {
            $building_id = Building::where('building_no',$datatables->request->get('building'))->first();
            if($datatables->request->get('status')) {
              $datatables->where('building_id', '=',$building_id->id)->where('status','=',$datatables->request->get('status'));
            }else {
              $datatables->where('building_id', '=',$building_id->id);
            }
          }
        if($datatables->request->get('status')) {
          if($datatables->request->get('quarter')) {
            $building_id = Building::where('name',$datatables->request->get('quarter'))->first();
            $datatables->where('status','=',$datatables->request->get('status'))->where('building_id', '=',$building_id->id);
          }else {
            $datatables->where('status','=',$datatables->request->get('status'));
          }
        }
        return $datatables->make(true);
    }

    public function removeStaff(Request $request,$id) {
    	$allocate = Allocate::find($id);
      $staff = Staff::find($allocate->staff_id);
      $staff->allocate = 2;
      $staff->save();
      $old_allocate = new OldAllocated;
      $old_allocate->building_id = $allocate->building_id;
      $old_allocate->housing_category_id = $allocate->housing_category_id;
      $old_allocate->quarter_id = $allocate->quarter_id;
      $old_allocate->ministry_id = $allocate->ministry_id;
      $old_allocate->room_no =  $allocate->room_no;
      $old_allocate->staff_id =  $staff->id;
      $old_allocate->remark = $allocate->remark;
      $old_allocate->ministry_id = $staff->ministry_id;
      $old_allocate->save();
    	$allocate->staff_id = null;
      $allocate->ministry_id = null;
    	$allocate->remark = null;
      $allocate->status = 2;
    	$allocate->save();
    	return redirect()->back()->withSuccess("ဝန်ထမ်းကို အခန်းမှ ဖယ်ထုတ်ပြီးပါပြီ");
    }
}
