<?php

namespace App\Http\Controllers\Married;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\LivingHousing;
class LivingHousingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('married_housings.living_housing.index';
    }
    public function living_housing() {
        $housing_category = HousingCategory::where('id',4)->first();
        $living_fours = HousingFour::where('allocate',2)->paginate(100);
        return view('married_housings.living_housing.list',compact('housing_category','living_fours'));
    }

}
