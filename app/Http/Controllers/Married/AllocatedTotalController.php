<?php

namespace App\Http\Controllers\Married;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\AllocatedCategory;
use App\AllocatedTotal;
use App\Allocate;
use App\HousingCategory;
use App\Ministry;
use DB;
class AllocatedTotalController extends Controller
{
    public function index() {
    	$results = AllocatedCategory::all();
    	
    	$categories = HousingCategory::where('id','>',3)->get(['id'])->toArray();

    	$housing_quarter_id = AllocatedTotal::whereIn('housing_category_id',$categories)->pluck('quarter_id');
        
    	$quarters = AllocatedTotal::whereIn('quarter_id',$housing_quarter_id)->whereIn('housing_category_id',$categories)->get();

    	$total_quarters = DB::table('total_quarters')->select('*')->get();

        $housing_four = Allocate::where('housing_category_id','=',4)->orderBy('quarter_id','asc')->groupBy('quarter_id')->get();
    	$housing_five = Allocate::where('housing_category_id','=',5)->orderBy('quarter_id','asc')->groupBy('quarter_id')->get();
        $housing_six = Allocate::where('housing_category_id','=',6)->orderBy('quarter_id','asc')->groupBy('quarter_id')->get();
        $housing_seven = Allocate::where('housing_category_id','=',7)->orderBy('quarter_id','asc')->groupBy('quarter_id')->get();
        $housing_eight = Allocate::where('housing_category_id','=',8)->orderBy('quarter_id','asc')->groupBy('quarter_id')->get();
    	$ministries = Ministry::all();

    	return view('married_housings.allocate.total',compact('results','quarters','housing_four','housing_five','housing_six','housing_seven','housing_eight','total_quarters','ministries'));
    }
}
