<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests\HouseCategoryRequest;
use App\HousingCategory;
use Datatables;
class HousingCategoryController extends Controller
{
  
    public function index() {        
        return view('married_housings.category.index');
    }

    public function create() {
        return view('married_housings.category.create',compact('category'));
    }

    public function store(HouseCategoryRequest $request) {  
        $housing_category = new HousingCategory;
        $housing_category->name = $request->name;
        $housing_category->short_name = $request->short_name;
        $housing_category->description = $request->description;
        return redirect('/housing_category')->with ('success','အိမ်ရာအမျိုးအစားအသစ်တစ်ခု ထည့်သွင်းပြီးပါပြီ');
    }

    public function edit($id) {
        $housing_category = HousingCategory::find($id); 
        return view('married_housings.category.edit' ,compact('housing_category'));
    }

    public function update(HouseCategoryRequest $request, $id) {   
        $housing_category = HousingCategory::find($id);  
        $housing_category->name = $request->name;
        $housing_category->short_name = $request->short_name;
        $housing_category->description = $request->description;
        $housing_category->save();
        return redirect('/housing_category')->with ('success','လုပ်ဆောင်ချက် အောင်မြင်ပါသည်');
    }

    public function destroy($id) {
        $housing_category = HousingCategory::find($id); 
        $housing_category->forceDelete();
        return redirect('/housing_category')->with('success','housing has been deleted successfully');
    }

    public function datatable() {
        $results = HousingCategory::query();
        $datatables = Datatables::eloquent($results)
        ->addIndexColumn()
        ->addColumn('action',
            function($results){
                $url = url('');
                $token = csrf_token();
                return '
                        <div class="action-buttons">
                          <a href="'.$url.'/housing_category/'.$results->id.'/edit" class="green">
                            <i class="ace-icon fa fa-pencil-square-o  bigger-130"></i>
                          </a>
                          <form action="'.$url.'/housing_category/'.$results->id.'" method="post" class="inline">
                              <input type="hidden" name="_method" value="DELETE">
                              <input type="hidden" name="_token" value="'.$token.'">
                              <a data-id="" class="red" data-toggle="modal" data-target="#confirmDelete">
                                <i class="ace-icon fa fa-trash-o bigger-130"></i>
                            </a>
                          </form>
                        </div>
                        ';
            });
        return $datatables->make(true);
    }
}
