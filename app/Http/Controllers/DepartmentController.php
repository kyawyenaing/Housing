<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests\DepartmentRequest;
use App\Department;
use App\Ministry;
use App\Staff;
use App\Allocate;
use Datatables;
class DepartmentController extends Controller
{
  
    public function index() {
        return view('department.index');
    }

    public function create() {
        $ministries = Ministry::all();
        return view('department.create',compact('ministries'));
    }

    public function store(DepartmentRequest $request) {  
        $department = new Department;
        $department->ministry_id = $request->ministry_id;
        $department->name = $request->name;
        $department->name_en = $request->name_en;
        $department->description = $request->description;
        $department->save();
        return redirect('/department')->with ('success','ဦးစီးဌာနသစ်တစ်ခု ထည့်သွင်းပြီးပါပြီ');
    }
   
    public function edit($id) {
        $department = Department::find($id); 
        return view('department.edit' ,compact('department'));
    }

  
    public function update(DepartmentRequest $request, $id) {   
        $department = Department::find($id);  
    
        $staffs = Staff::where('department_id',$id)->get();
            foreach($staffs as $staff) {
                $staff->ministry_id = $request->ministry_id;
                $staff->save();
            }
        $allocate = Allocate::where('ministry_id',$department->ministry_id)->get();
            foreach($allocate as $result) {
                $result->ministry_id = $request->ministry_id;
                $result->save();
            }

        $department->ministry_id = $request->ministry_id;
        $department->name = $request->name;
        $department->name_en = $request->name_en;
        $department->description = $request->description;
        $department->save();
        return redirect('/department')->with ('success','ဦးစီးဌာန၏ အချက်အလက်များကို ပြင်ဆင်ပြီးပါပြီ');
    }

    public function destroy($id) {
        $department = Department::find($id); 
        $department->forceDelete();
        return redirect('/department')->with('success','Department has been deleted successfully');
    }

    public function datatable() {
        $results = Department::with('ministry');
        $datatables = Datatables::eloquent($results)
                ->addIndexColumn()
                ->addColumn('action',function($results){
                $url = url('');
                $token = csrf_token();
        return '
        <div class="action-buttons">
          <a href="'.$url.'/department/'.$results->id.'/edit" class="green">
            <i class="ace-icon fa fa-pencil-square-o  bigger-130"></i>
          </a>
          <form action="'.$url.'/department/'.$results->id.'" method="post" class="inline">
              <input type="hidden" name="_method" value="DELETE">
              <input type="hidden" name="_token" value="'.$token.'">
              <a data-id="" class="red" data-toggle="modal" data-target="#confirmDelete">
                <i class="ace-icon fa fa-trash-o bigger-130"></i>
            </a>
          </form>
        </div>
        ';
      });
        return $datatables->make(true);
    }
}
