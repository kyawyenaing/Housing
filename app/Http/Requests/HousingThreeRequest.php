<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\validation\Rule;

class HousingThreeRequest extends FormRequest
{
    public function authorize() {
        return true;
    }

    public function rules() {
       return [
            'quarter_id' =>'required',
            'building_id' => 'required',
            'room_no' => 'required',
        ];
    }
    public function messages() {
      return[
        'quarter_id.required' => 'အိမ်ရာတည်ရှိရာနေရာကို ရွေးချယ်ပေးပါ',
        'building_id.required' => 'တိုက်အမှတ်ကို ရိုက်ထည့်ပေးပါ',
        'room_no.required' => 'အခန်းအမှတ်ကို ရိုက်ထည့်ပေးပါ',
     ];
    }
}

