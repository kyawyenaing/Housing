<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
class BuildingRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }
    public function rules()
    {
        return [
            'building_no' => 'required',
        ];
    }
    public function messages()
     {
      return[
        'building_no.required' => 'အဆောက်အဦးအမှတ် (တိုက်အမှတ်) ကိုရိုက်ထည့်ပေးပါ',
        ];
    }
}
