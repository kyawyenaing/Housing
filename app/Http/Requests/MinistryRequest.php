<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class MinistryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'name_en' => 'required',
        ];
    }
      public function messages()
     {
      return[
        'name.required' => 'ဝန်ကြီးဌာနအမည်(Myanmar) ကိုရိုက်ထည့်ပါ',
        'name_en.required' => 'ဝန်ကြီးဌာနအမည်(English) ကိုရိုက်ထည့်ပါ',

    ];
}
}
