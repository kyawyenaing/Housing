<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RankRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules ()
    {
        return [
            //
            'name_en' => 'required',
            'name' => 'required',
            'description' => 'required',

        ];
    }
    public function messages () 
    {
        return [
        'name_en.required' => 'Name_English is required',
        'name.required' => 'Name is required',
        'description.required' => 'description is required',
        ];
    }
}
