<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class DepartmentRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }
    public function rules()
    {
        return [
            'name' => 'required',
            'name_en' => 'required',
        ];
    }
    public function messages()
     {
      return[
        'name.required' => 'ဦးစီးဌာနအမည်(Myanmar) ကိုရိုက်ထည့်ပေးပါ',
        'name_en.required' => 'ဦးစီးဌာနအမည်(English) ကိုရိုက်ထည့်ပေးပါ',
        ];
    }
}
