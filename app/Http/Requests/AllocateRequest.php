<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
class AllocateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }
    public function rules()
    {
        return [
            'room_no.*' => 'required',
            'staff_id.*' => 'required',
        ];
    }
    public function messages()
     {
      return[
        'room_no.*.required' => 'ဦးစီးဌာနအမည်(Myanmar) ကိုရိုက်ထည့်ပေးပါ',
        'staff_id.*.required' => 'ဦးစီးဌာနအမည်(English) ကိုရိုက်ထည့်ပေးပါ',
        ];
    }
}
