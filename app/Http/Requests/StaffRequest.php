<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StaffRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
     public function rules ()
    {
        return [
            'name' => 'required',
            'name_en' => 'required',
            'position_id' => 'required',
            'department_id' => 'required',
            // 'ministry_id' => 'required',
            'address' => 'required',
        ];
    }
    public function messages () 
    {
        return [
        'name.required' => 'ဝန်ထမ်းအမည်ကိုရိုက်ထည့်ပေးပါ',
        'name_en.required' => ' ဝန်ထမ်းအမည်(English) ကိုရိုက်ထည့်ပေးပါ',
        'position_id.required' => 'ရာထူးကိုရွေးချယ်ပေးပါ',
        'department_id.required' => 'ဦးစီးဌာနကိုရွေးချယ်ပေးပါ',
        // 'ministry_id.required' => 'ဝန်ကြီးဌာနကိုရွေးချယ်ပေးပါ',
        'address.required' => 'လိပ်စာကိုရိုက်ထည့်ပေးပါ',
        ];
    }
}