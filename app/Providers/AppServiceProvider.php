<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use DB;
use Illuminate\Support\Facades\View;
use App\Staff;
use App\Building;
use App\Department;
use Illuminate\Database\Eloquent\Model;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Schema::defaultstringlength(191);

         $departments = Department::with('ministry')->get();
         
         $ministries = DB::table('ministries')
        ->orderBy('updated_at','desc') 
        ->get();

         $positions = DB::table('positions')
        ->orderBy('updated_at','desc') 
        ->get();

        $housing_categories = DB::table("housing_categories")->get();
        $quarters = DB::table("quarters")->get();
        $buildings = Building::all();

        View::share('departments', $departments);
        View::share('ministries', $ministries);
        View::share('positions', $positions);
        View::share('housing_categories', $housing_categories);
        View::share('quarters', $quarters);                  
        View::share('buildings', $buildings);         
        
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
