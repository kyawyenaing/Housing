<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Building extends Model
{
    //
    protected $table="buildings";
    public function housing_category() {
    	return $this->belongsto('App\HousingCategory');
    }
    public function quarter() {
    	return $this->belongsto('App\Quarter');
    }
}
