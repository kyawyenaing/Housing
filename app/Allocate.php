<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Allocate extends Model
{
    //
    protected $table = "allocates";
    public function housing_category() {
    	return $this->belongsto('App\HousingCategory');
    }
    public function building() {
    	return $this->belongsto('App\Building');
    }public function quarter() {
        return $this->belongsto('App\Quarter');
    }
    public function ministry() {
    	return $this->belongsto('App\Ministry');
    }
    public function staff() {
        return $this->belongsto('App\Staff');
    }
}
