<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class HousingCategory extends Model
{
     use Notifiable;


    protected $table="housing_categories";
}
