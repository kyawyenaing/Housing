<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{

    public function department (){
        return $this->belongsTo('App\Department');
    }
    public function position (){
        return $this->belongsTo('App\Position');
    }
    public function ministry (){
        return $this->belongsTo('App\Ministry');
    }
    public function rank (){
        return $this->belongsTo('App\Rank');
    }
    
    protected $table="staffs";

}
