<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Ministry;
class AllocatedTotal extends Model
{
    // 
    protected $table = "total_results";
    public function ministry() {
    	return $this->belongsto('App\Ministry');
    }

    public function get_ministry($id) {
    	$ministry = Ministry::fine($id);
    	return $ministry->name;
    }
    public function count_one($category,$ministry) {
    	$category_one = DB::table('allocates')->select('id')->where('housing_category_id',$category)->where('ministry_id',$ministry);
    	return $category_one->count();
    }

    public function total_quarter($ministry) {
        $four_sum = DB::table('total_results')->where('ministry_id',$ministry)->where('housing_category_id','=',4)->sum('count_category');
        return $four_sum;
    }
}
