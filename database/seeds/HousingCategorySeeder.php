<?php

use Illuminate\Database\Seeder;

class HousingCategorySeeder extends Seeder
{

    public function run()
    {
         DB::table('housing_categories')->insert([
        	[
	            'name' => '၀န်ကြီး',
	            'short_name' => "Ministry",
	            'description' => "Ministry",
	            'created_at' => new DateTime(),
	            'updated_at' => new DateTime(),
	        ],
	        [
	            'name' => 'ဒုဝန်ကြီး',
	            'short_name' => "Deputy Minister",
	            'description' => "Deputy Minister",
	            'created_at' => new DateTime(),
	            'updated_at' => new DateTime(),
	        ],
	        [
	            'name' => 'Duplex',
	            'short_name' => "Duplex",
	            'description' => "Duplex",	
	            'created_at' => new DateTime(),
	            'updated_at' => new DateTime(),
	        ],
	        [
	            'name' => '၂ခန်းတွဲ ၃ထပ်- အိပ်ခန်း(၃)ခန်း',
	            'short_name' => "2U3S",
	            'description' => "2U3S",
	            'created_at' => new DateTime(),
	            'updated_at' => new DateTime(),
	        ],
	        [
	            'name' => '၄ခန်းတွဲ ၄ထပ်- အိပ်ခန်း(၃)ခန်း',
	            'short_name' => "4U4S(3B)",
	            'description' => "4U4S",
	            'created_at' => new DateTime(),
	            'updated_at' => new DateTime(),
	        ],
	        [
	            'name' => '၆ခန်းတွဲ ၄ထပ်- အိပ်ခန်း၃ခန်း',
	            'short_name' => "6U4S(3B)",
	            'description' => "6U4S",
	            'created_at' => new DateTime(),
	            'updated_at' => new DateTime(),
	        ],
	        [
	            'name' => '၄ခန်းတွဲ ၄ထပ်- အိပ်ခန်း(၂)ခန်း',
	            'short_name' => "4U4S(2B)",
	            'description' => "4U4S(2B)",
	            'created_at' => new DateTime(),
	            'updated_at' => new DateTime(),
	        ],
	        [
	            'name' => '၆ခန်းတွဲ ၄ထပ်- အိပ်ခန်း(၂)ခန်း',
	            'short_name' => "6U4S(2B)",
	            'description' => "6U4S(2B)",
	            'created_at' => new DateTime(),
	            'updated_at' => new DateTime(),
	        ],
	    ]);
    }
}
