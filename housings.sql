-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 07, 2018 at 02:42 ညနေ
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `housings`
--

-- --------------------------------------------------------

--
-- Table structure for table `allocates`
--

CREATE TABLE `allocates` (
  `id` int(10) NOT NULL,
  `building_id` int(11) DEFAULT NULL,
  `room_no` varchar(255) NOT NULL,
  `housing_category_id` int(11) NOT NULL,
  `quarter_id` int(11) NOT NULL,
  `staff_id` int(11) DEFAULT NULL,
  `ministry_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `remark` text,
  `remark_by_ministry` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `allocates`
--

INSERT INTO `allocates` (`id`, `building_id`, `room_no`, `housing_category_id`, `quarter_id`, `staff_id`, `ministry_id`, `status`, `remark`, `remark_by_ministry`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'a-1 edit', 4, 1, 4, 5, 1, 'dit', NULL, '2018-06-06 04:42:23', '2018-06-05 22:12:23', NULL),
(2, 1, 'a-2', 4, 1, 2, 1, 1, 'a-2 kkh', NULL, '2018-06-06 04:42:23', '2018-06-05 22:12:23', NULL),
(3, 2, '003', 1, 2, 3, 1, 1, 'haha', NULL, '2018-06-05 21:29:50', '2018-06-05 21:29:50', NULL),
(4, 2, 'a-1', 1, 2, NULL, NULL, 2, NULL, NULL, '2018-06-06 04:43:42', '2018-06-05 22:13:42', NULL),
(5, 3, '0005', 1, 5, 1, 1, 1, 'Ato', NULL, '2018-06-05 22:15:12', '2018-06-05 22:15:12', NULL),
(6, 3, 'DNT-2', 1, 5, 5, 1, 1, 'asdf', NULL, '2018-06-05 22:24:11', '2018-06-05 22:24:11', NULL),
(7, 4, 'B-2', 4, 4, 6, 1, 1, 'asdf', NULL, '2018-06-05 23:57:49', '2018-06-05 23:57:49', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `buildings`
--

CREATE TABLE `buildings` (
  `id` int(10) NOT NULL,
  `building_no` varchar(255) NOT NULL,
  `housing_category_id` int(11) DEFAULT NULL,
  `quarter_id` int(11) NOT NULL,
  `description` longtext,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `buildings`
--

INSERT INTO `buildings` (`id`, `building_no`, `housing_category_id`, `quarter_id`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'A-1', 4, 1, 'First Building', '2018-06-06 04:42:23', '2018-06-05 22:12:23', NULL),
(2, 'Building-One', 1, 2, 'Building-One', '2018-06-05 21:29:32', '2018-06-05 21:29:32', NULL),
(3, 'B-2', 1, 5, 'B-2', '2018-06-05 22:14:07', '2018-06-05 22:14:07', NULL),
(4, 'B-1', 4, 4, 'B-1', '2018-06-05 23:56:38', '2018-06-05 23:56:38', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` int(10) NOT NULL,
  `ministry_id` int(11) NOT NULL,
  `name` longtext,
  `name_en` longtext,
  `description` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `ministry_id`, `name`, `name_en`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'သမ္မတရုံး', 'President  Office', 'President  Office', '2018-05-17 03:27:48', '2018-06-05 10:11:45', NULL),
(2, 2, 'အစိုးရအဖွဲ့ရုံး', 'အစိုးရအဖွဲ့ရုံး', 'အစိုးရအဖွဲ့ရုံး', '2018-05-17 03:48:37', '2018-05-28 21:29:44', NULL),
(3, 3, 'လွတ်တော်ရုံး', 'လွတ်တော်ရုံး', 'လွတ်တော်ရုံး', '2018-05-17 03:50:27', '2018-05-28 21:30:06', NULL),
(4, 5, 'ဖွဲ့စည်းပုံခုံရုံး', 'ဖွဲ့စည်းပုံခုံရုံး', 'ဖွဲ့စည်းပုံခုံရုံး', '2018-05-28 21:20:55', '2018-05-28 21:31:16', NULL),
(5, 8, 'ကာကွယ်ရေး', 'ကာကွယ်ရေး', 'ကာကွယ်ရေး', '2018-05-28 21:25:38', '2018-05-28 21:32:16', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `housing_categories`
--

CREATE TABLE `housing_categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `short_name` varchar(255) DEFAULT NULL,
  `description` longtext,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `housing_categories`
--

INSERT INTO `housing_categories` (`id`, `name`, `short_name`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '၀န်ကြီး', 'Ministry', 'Ministry', '2018-05-28 21:15:08', '2018-05-28 21:15:08', NULL),
(2, 'ဒုဝန်ကြီး', 'Deputy Minister', 'Deputy Minister', '2018-05-28 21:15:08', '2018-05-28 21:15:08', NULL),
(3, 'Duplex', 'Duplex', 'Duplex', '2018-05-28 21:15:08', '2018-05-28 21:15:08', NULL),
(4, '၂ခန်းတွဲ ၃ထပ်- အိပ်ခန်း(၃)ခန်း', '2U3S', '2U3S', '2018-05-28 21:15:08', '2018-05-28 21:15:08', NULL),
(5, '၄ခန်းတွဲ ၄ထပ်- အိပ်ခန်း(၃)ခန်း', '4U4S(3B)', '4U4S', '2018-05-28 21:15:08', '2018-05-28 21:15:08', NULL),
(6, '၆ခန်းတွဲ ၄ထပ်- အိပ်ခန်း၃ခန်း', '6U4S(3B)', '6U4S', '2018-05-28 21:15:08', '2018-05-28 21:15:08', NULL),
(7, '၄ခန်းတွဲ ၄ထပ်- အိပ်ခန်း(၂)ခန်း', '4U4S(2B)', '4U4S(2B)', '2018-05-28 21:15:08', '2018-05-28 21:15:08', NULL),
(8, '၆ခန်းတွဲ ၄ထပ်- အိပ်ခန်း(၂)ခန်း', '6U4S(2B)', '6U4S(2B)', '2018-05-28 21:15:08', '2018-05-28 21:15:08', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `housing_ones`
--

CREATE TABLE `housing_ones` (
  `id` int(11) NOT NULL,
  `housing_category_id` int(11) DEFAULT '1',
  `building_id` int(11) NOT NULL,
  `room_no` varchar(255) NOT NULL,
  `quarter_id` int(11) NOT NULL,
  `description` longtext,
  `allocate` int(11) NOT NULL DEFAULT '1',
  `staff_id` int(11) DEFAULT NULL,
  `ministry_id` int(11) DEFAULT NULL,
  `living_date` date DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `delected_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `housing_ones`
--

INSERT INTO `housing_ones` (`id`, `housing_category_id`, `building_id`, `room_no`, `quarter_id`, `description`, `allocate`, `staff_id`, `ministry_id`, `living_date`, `created_at`, `updated_at`, `delected_at`) VALUES
(1, 1, 1, 'Room-1 Edit', 1, 'Room-1Room-1', 2, 1, 1, '2018-02-26', '2018-02-25 07:07:16', '2018-02-25 00:37:16', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(3, '2014_10_12_000000_create_users_table', 1),
(4, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ministries`
--

CREATE TABLE `ministries` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `name_en` varchar(255) NOT NULL,
  `prior` int(11) DEFAULT NULL,
  `description` text,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL,
  `delected_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ministries`
--

INSERT INTO `ministries` (`id`, `name`, `name_en`, `prior`, `description`, `updated_at`, `created_at`, `delected_at`) VALUES
(1, 'သမ္မတရုံး', 'Presidence''s Office', NULL, 'သမ္မတရုံး', '2018-05-17 02:46:59', '2018-05-17 02:46:59', NULL),
(2, 'အစိုးရအဖွဲ့ရုံး', 'Government Office', NULL, 'အစိုးရအဖွဲ့ရုံး', '2018-05-17 02:46:59', '2018-05-17 02:46:59', NULL),
(3, 'လွှတ်တော်ရုံး', 'Parliamentary Office', NULL, 'လွှတ်တော်ရုံး', '2018-05-17 02:46:59', '2018-05-17 02:46:59', NULL),
(4, 'တရားလွှတ်တော်ချုပ်', 'Supreme Court', NULL, 'တရားလွှတ်တော်ချုပ်', '2018-05-17 02:46:59', '2018-05-17 02:46:59', NULL),
(5, 'အခြေခံဥပဒေခုံရုံး', 'The constitutional tribunal', NULL, 'အခြေခံဥပဒေခုံရုံး', '2018-05-17 02:46:59', '2018-05-17 02:46:59', NULL),
(6, 'ကော်မရှင်ရုံး', 'Commission  Office', NULL, 'ကော်မရှင်ရုံး', '2018-05-17 02:46:59', '2018-05-17 02:46:59', NULL),
(7, 'ပြည်ထဲရေး', 'Interior', NULL, 'ပြည်ထဲရေး', '2018-05-17 02:46:59', '2018-05-17 02:46:59', NULL),
(8, 'ကာကွယ်ရေး', 'Defence', NULL, 'ကာကွယ်ရေး', '2018-05-17 02:46:59', '2018-05-17 02:46:59', NULL),
(9, 'နယ်စပ်ရေးရာ', 'Border', NULL, 'နယ်စပ်ရေးရာ', '2018-05-17 02:46:59', '2018-05-17 02:46:59', NULL),
(10, 'နိုင်ငံတော်အတိုင်ပင်ခံရုံ', '', NULL, 'နိုင်ငံတော်အတိုင်ပင်ခံရုံး', '2018-05-17 02:46:59', '2018-05-17 02:46:59', NULL),
(11, 'နိုင်ငံခြားရေး', 'Foreign', NULL, 'နိုင်ငံခြားရေး', '2018-05-17 02:46:59', '2018-05-17 02:46:59', NULL),
(12, 'ပြန်ကြားရေး', 'Information', NULL, 'ပြန်ကြားရေး', '2018-05-17 02:46:59', '2018-05-17 02:46:59', NULL),
(13, 'သာသနာရေးနှင့် ယဉ်ကျေးမှု', 'Religious Affairs and Culture', NULL, 'သာသနာရေးနှင့် ယဉ်ကျေးမှု', '2018-05-17 02:46:59', '2018-05-17 02:46:59', NULL),
(14, 'စိုက်ပျိုးရေး၊ မွေးမြူရေး နှင့် ဆည်မြောင်း', 'agriculture, Livestock and Irrigation', NULL, 'စိုက်ပျိုးရေး၊ မွေးမြူရေး နှင့် ဆည်မြောင်း', '2018-05-17 02:46:59', '2018-05-17 02:46:59', NULL),
(15, 'ပို့ဆောင်ရေးနှင့် ဆက်သွယ်ရေး', 'Transport and communications', NULL, 'ပို့ဆောင်ရေးနှင့် ဆက်သွယ်ရေး', '2018-05-17 02:46:59', '2018-05-17 02:46:59', NULL),
(16, 'သယံဇာတနှင့် သဘာဝပတ်ဝန်းကျင်', 'Natural Resources and Environment', NULL, 'သယံဇာတနှင့် သဘာဝပတ်ဝန်းကျင်', '2018-05-17 02:46:59', '2018-05-17 02:46:59', NULL),
(17, 'လျှပ်စစ်နှင့်စွမ်းအင်', 'Electricity and Energy', NULL, 'လျှပ်စစ်နှင့်စွမ်းအင်', '2018-05-17 02:46:59', '2018-05-17 02:46:59', NULL),
(18, 'အလုပ်သမားနှင့် လူဝင်မှုကြီးကြပ်ရေး', 'Labor and Immigration', NULL, 'အလုပ်သမားနှင့် လူဝင်မှုကြီးကြပ်ရေး', '2018-05-17 02:46:59', '2018-05-17 02:46:59', NULL),
(19, 'စက်မှု', ' Industry', NULL, 'စက်မှု', '2018-05-17 02:46:59', '2018-05-17 02:46:59', NULL),
(20, 'စီးပွါးရေးနှင့် ကူးသန်ရောင်းဝယ်ရေး', 'Business / Commerce', NULL, 'စီးပွား/ကူးသန်း', '2018-05-17 02:46:59', '2018-05-17 02:46:59', NULL),
(21, 'ကျန်းမာရေးနှင့် အားကစား ', ' Health and Sport', NULL, 'ကျန်းမာရေးနှင့် အားကစား ', '2018-05-17 02:46:59', '2018-05-17 02:46:59', NULL),
(22, 'ပညာရေး', 'Education', NULL, 'ပညာရေး', '2018-05-17 02:46:59', '2018-05-17 02:46:59', NULL),
(23, 'စီမံကိန်းနှင့် ဘဏ္ဍာရေး', 'Planning and Finance', NULL, 'စီမံကိန်းနှင့် ဘဏ္ဍာရေး', '2018-05-17 02:46:59', '2018-05-17 02:46:59', NULL),
(24, 'ဆောက်လုပ်ရေး', 'Construction', NULL, 'ဆောက်လုပ်ရေး', '2018-05-17 02:46:59', '2018-05-17 02:46:59', NULL),
(25, 'လူမှုဝန်ထမ်း', 'Social welfare', NULL, 'လူမှုဝန်ထမ်း', '2018-05-17 02:46:59', '2018-05-17 02:46:59', NULL),
(26, 'ဟိုတယ်နှင့်ခရီးသွားလာရေ', 'Hotels and Tourism', NULL, 'ဟိုတယ်ခရီး', '2018-05-17 02:46:59', '2018-05-17 02:46:59', NULL),
(27, 'တိုင်းရင်းသားလူမျိုးများရေးရာ', '', NULL, 'တိုင်းရင်းသားလူမျိုးများရေးရာ', '2018-05-17 02:46:59', '2018-05-17 02:46:59', NULL),
(28, 'အပြည်ပြည်ဆိုင်ရာပူးပေါင်းဆောင်ရွက်ရေး', '', NULL, 'အပြည်ပြည်ဆိုင်ရာပူးပေါင်းဆောင်ရွက်ရေး', '2018-05-17 02:46:59', '2018-05-17 02:46:59', NULL),
(29, 'ရှေ့နေချုပ်ရုံး', 'Attorney General''s Office', NULL, 'ရှေ့နေချုပ်ရုံး', '2018-05-17 02:46:59', '2018-05-17 02:46:59', NULL),
(30, 'စာရင်းစစ်ချုပ်ရုံး', 'Office of the Auditor', NULL, 'စာရင်းစစ်ချုပ်ရုံး', '2018-05-17 02:46:59', '2018-05-17 02:46:59', NULL),
(31, 'ရာထူးဝန်အဖွဲ့', 'Civil Services Board', NULL, 'ရာထူးဝန်အဖွဲ့', '2018-05-17 02:46:59', '2018-05-17 02:46:59', NULL),
(32, 'ဗဟိုဘဏ်', 'Central Bank', NULL, 'ဗဟိုဘဏ်', '2018-05-17 02:46:59', '2018-05-17 02:46:59', NULL),
(33, 'အဂတိလိုက်စားမှုတိုက်ဖျက်ရေး', 'Anti-corruption', NULL, 'အဂတိလိုက်စားမှုတိုက်ဖျက်ရေး', '2018-05-17 02:46:59', '2018-05-17 02:46:59', NULL),
(34, 'ခရိုင်ဥပဒေရုံး', 'District Legal Office', NULL, 'ခရိုင်ဥပဒေရုံး', '2018-05-17 02:46:59', '2018-05-17 02:46:59', NULL),
(35, 'ဇမ္ဗူသီရိ၊ခုတင်-၅၀', '', NULL, 'ဇမ္ဗူသီရိ၊ခုတင်-၅၀', '2018-05-17 02:46:59', '2018-05-17 02:46:59', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `old_allocates`
--

CREATE TABLE `old_allocates` (
  `id` int(10) NOT NULL,
  `building_id` int(11) DEFAULT NULL,
  `room_no` varchar(255) NOT NULL,
  `housing_category_id` int(11) NOT NULL,
  `quarter_id` int(11) NOT NULL,
  `staff_id` int(11) NOT NULL,
  `ministry_id` int(11) NOT NULL,
  `remark` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `old_allocates`
--

INSERT INTO `old_allocates` (`id`, `building_id`, `room_no`, `housing_category_id`, `quarter_id`, `staff_id`, `ministry_id`, `remark`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'a-1', 4, 2, 1, 1, 'ATO', '2018-06-05 11:12:41', '2018-06-05 11:12:41', NULL),
(2, 2, 'a-1', 1, 2, 1, 1, NULL, '2018-06-05 22:13:42', '2018-06-05 22:13:42', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `positions`
--

CREATE TABLE `positions` (
  `id` int(10) NOT NULL,
  `name` longtext,
  `name_en` longtext,
  `description` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `positions`
--

INSERT INTO `positions` (`id`, `name`, `name_en`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'အမြဲတမ်းအတွင်းဝန် (တာဝန်ခံညွှန်ကြားရေးမှူးချုပ်)', NULL, '0', '2018-03-15 16:20:06', '2018-03-20 21:55:03', NULL),
(2, 'ဌာနကြီးမှုး(ညွှန်ကြားရေးမှုးချုပ်)', NULL, '0', '2018-03-15 16:20:39', '2018-03-20 21:13:05', NULL),
(3, 'ဒုတိယအတွင်းဝန်', NULL, '33', '2018-03-15 16:21:16', '2018-03-15 16:21:16', NULL),
(4, 'ဌာနမှုး', NULL, '33', '2018-03-15 16:21:37', '2018-03-15 16:21:37', NULL),
(5, 'ဒု-ညွှန်ချုပ်', NULL, '33', '2018-03-15 16:21:53', '2018-03-15 16:21:53', NULL),
(6, 'စက်ရုံမှုး', NULL, '33', '2018-03-15 16:22:13', '2018-03-15 16:22:13', NULL),
(7, 'ဆေးရုံအုပ်ကြီး', NULL, '33', '2018-03-15 16:22:53', '2018-03-15 16:22:53', NULL),
(8, 'အထူးကုဆရာဝန်ကြီး', NULL, '33', '2018-03-15 16:23:16', '2018-03-15 16:23:16', NULL),
(9, 'လက်ထောက်အတွင်းဝန်', NULL, '58', '2018-03-15 16:23:58', '2018-03-15 16:23:58', NULL),
(10, 'ဒု-ဌာနမှုး', NULL, '58', '2018-03-15 16:24:14', '2018-03-15 16:24:14', NULL),
(11, 'ညွှန်မှုး', NULL, '58', '2018-03-15 16:24:37', '2018-03-15 16:24:37', NULL),
(12, 'ဒု-စက်ရုံမှုး', NULL, '58', '2018-03-15 16:24:58', '2018-03-15 16:24:58', NULL),
(13, 'ဒု-ဆေးရုံအုပ်ကြီး', NULL, '58', '2018-03-15 16:25:35', '2018-03-15 16:25:35', NULL),
(14, 'လ/ထ ဌာနမှုး', NULL, '115', '2018-03-15 16:26:14', '2018-03-15 16:26:14', NULL),
(15, 'ဒုညွှန်မှုး', NULL, '115', '2018-03-15 16:26:44', '2018-03-15 16:26:44', NULL),
(16, 'လ/ထ စက်ရုံမှုး', NULL, '115', '2018-03-15 16:26:59', '2018-03-15 16:26:59', NULL),
(17, 'လ/ထ ဆေးရုံအုပ်ကြီး', NULL, '115', '2018-03-15 16:27:34', '2018-03-15 16:27:34', NULL),
(18, 'အထူးကုဆရာဝန်', NULL, '0', '2018-03-15 16:28:02', '2018-03-15 16:28:02', NULL),
(19, 'ဌာနခွဲမှုး', NULL, '186', '2018-03-15 16:28:18', '2018-03-15 16:28:18', NULL),
(20, 'လ/ထ ညွှန်မှုး', NULL, '186', '2018-03-15 16:28:36', '2018-03-15 16:28:36', NULL),
(21, 'အလုပ်ရုံမှုး', NULL, '186', '2018-03-15 16:29:17', '2018-03-15 16:29:17', NULL),
(22, 'ဆက်ဆံရေးအရာရှိ', NULL, '186', '2018-03-15 16:29:34', '2018-03-15 16:29:34', NULL),
(23, 'သူနာပြုအုပ်ကြီး', NULL, '186', '2018-03-15 16:29:53', '2018-03-15 16:29:53', NULL),
(24, 'ဦးစီးအရာရှိ', NULL, '393', '2018-03-15 16:30:17', '2018-03-15 16:30:17', NULL),
(25, 'လ/ထ အင်ဂျင်နီယာ', NULL, '393', '2018-03-15 16:30:38', '2018-03-15 16:30:38', NULL),
(26, 'အလုပ်ရုံခွဲမှုး', NULL, '393', '2018-03-15 16:31:18', '2018-03-15 16:31:18', NULL),
(27, 'လ/ထ ဆရာဝန်', NULL, '393', '2018-03-15 16:32:39', '2018-03-15 16:32:39', NULL),
(28, 'သူနာပြုအုပ်', NULL, '393', '2018-03-15 16:33:00', '2018-03-15 16:33:00', NULL),
(29, 'ရဲအုပ်', NULL, '5', '2018-03-15 16:37:25', '2018-03-15 16:37:25', NULL),
(30, 'ရုံးအုပ်', NULL, '250', '2018-03-15 16:42:28', '2018-03-15 16:42:28', NULL),
(31, 'ကျွမ်းကျင်-၁', NULL, '250', '2018-03-15 16:42:49', '2018-03-15 16:42:49', NULL),
(32, 'အလုပ်ရုံစုမှူး', NULL, '250', '2018-03-15 16:43:02', '2018-03-15 16:43:02', NULL),
(33, 'ဌာနခွဲစာရေး', NULL, '679', '2018-03-15 16:43:18', '2018-03-15 16:43:18', NULL),
(34, 'ငယ်/ယာ-၂', NULL, '679', '2018-03-15 16:43:32', '2018-03-15 16:43:32', NULL),
(35, 'အလုပ်ရုံစိတ်မှူး', NULL, '679', '2018-03-15 16:43:49', '2018-03-15 16:43:49', NULL),
(36, 'သူနာပြု-၂', NULL, '679', '2018-03-15 16:44:06', '2018-03-15 16:44:06', NULL),
(37, 'အကြီးတန်းစာရေး', NULL, '1085', '2018-03-15 16:44:21', '2018-03-15 16:44:21', NULL),
(38, 'ငယ်/ယာ-၃', NULL, '1085', '2018-03-15 16:44:41', '2018-03-15 16:44:41', NULL),
(39, 'ထုတ်ကျွမ်း-၃', NULL, '1085', '2018-03-15 16:45:10', '2018-03-15 16:45:10', NULL),
(40, 'သူနာပြု-၃', NULL, '1085', '2018-03-15 16:45:28', '2018-03-15 16:45:28', NULL),
(41, 'အငယ်တန်းစာရေး', NULL, '1399', '2018-03-15 16:45:44', '2018-03-20 23:20:50', NULL),
(42, 'ငယ်/ယာ-၄', NULL, '1399', '2018-03-15 16:46:07', '2018-03-20 23:20:50', NULL),
(43, 'ထုတ်ကျွမ်း-၄', NULL, '1399', '2018-03-15 16:46:29', '2018-03-20 23:20:50', NULL),
(44, 'သူနာပြု-၄', NULL, '1399', '2018-03-15 16:46:47', '2018-03-20 23:20:50', NULL),
(45, 'လုပ်သား(၁)', NULL, '1399', '2018-03-15 16:47:09', '2018-03-20 23:20:50', NULL),
(46, 'အမှုတွဲထိန်း', NULL, '1134', '2018-03-15 16:47:24', '2018-03-20 23:14:41', NULL),
(47, 'ထုတ်ကျွမ်း-၅', NULL, '1134', '2018-03-15 16:47:43', '2018-03-20 23:14:41', NULL),
(48, 'လုပ်သား(၂)', NULL, '1134', '2018-03-15 16:48:02', '2018-03-20 23:14:41', NULL),
(49, 'စာပို့', NULL, '746', '2018-03-15 16:48:19', '2018-03-20 23:20:49', NULL),
(50, 'အထွေထွေလုပ်သား', NULL, '746', '2018-03-15 16:48:33', '2018-03-20 23:20:49', NULL),
(51, 'လုပ်သား(၃)', NULL, '746', '2018-03-15 16:48:48', '2018-03-20 23:20:49', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `quarters`
--

CREATE TABLE `quarters` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `short_name` varchar(255) DEFAULT NULL,
  `description` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `quarters`
--

INSERT INTO `quarters` (`id`, `name`, `short_name`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'ဇေယျသိဒ္ဓိရပ်ကွက်', 'စံပယ်', 'စံပယ်', '2018-05-21 04:42:00', '2018-05-20 22:12:00', NULL),
(2, 'မင်္ဂလာသိဒ္ဓိရပ်ကွက်', 'ကံ့ကော်', 'ကံ့ကော်\r\nကံ့ကော်', '2018-05-21 04:42:46', '2018-05-20 22:12:46', NULL),
(3, 'ဘောဂသိဒ္ဓိရပ်ကွက်', 'ပိတောက်', 'ပိတောက်\r\nပိတောက်', '2018-05-21 04:43:54', '2018-05-20 22:13:54', NULL),
(4, 'ပညာသိဒ္ဓိရပ်ကွက်', 'ခရေ', 'ခရေ\r\nခရေ\r\nခရေ', '2018-05-21 04:44:37', '2018-05-20 22:14:37', NULL),
(5, 'ဓနသိဒ္ဓိရပ်ကွက်', 'လုပ်ကွက်အမှတ် (၁)', 'P-1', '2018-05-21 04:50:14', '2018-05-20 22:20:14', NULL),
(6, 'ဗလသိဒ္ဓိရပ်ကွက်', 'လုပ်ကွက်အမှတ် (၂)', 'P-2', '2018-05-21 04:49:37', '2018-05-20 22:19:37', NULL),
(7, 'ဉာဏသိဒ္ဓိရပ်ကွက်', 'လုပ်ကွက်အမှတ် (၃)', 'P-3', '2018-05-21 04:51:05', '2018-05-20 22:21:05', NULL),
(8, 'သုခသိဒ္ဓိရပ်ကွက်', 'လုပ်ကွက်အမှတ် (၄)', 'P-4', '2018-05-21 04:50:37', '2018-05-20 22:20:37', NULL),
(9, 'ဝဏ္ဏသိဒ္ဓိရပ်ကွက်', 'လုပ်ကွက်အမှတ် (၅)', 'P-5', '2018-05-21 04:49:00', '2018-05-20 22:19:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `staffs`
--

CREATE TABLE `staffs` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_en` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position_id` int(11) UNSIGNED NOT NULL,
  `department_id` int(11) UNSIGNED NOT NULL,
  `ministry_id` int(11) UNSIGNED NOT NULL,
  `address` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `allocate` int(11) NOT NULL DEFAULT '2',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `staffs`
--

INSERT INTO `staffs` (`id`, `name`, `name_en`, `position_id`, `department_id`, `ministry_id`, `address`, `allocate`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'ဦးအောင်သန်းဦး', 'U Aung Than Oo', 5, 1, 1, 'First', 1, '2018-05-21 07:50:56', '2018-06-05 22:15:12', NULL),
(2, 'ဦးကျော်ကျော်ဟန်', 'U Kyaw Kyaw Han', 5, 1, 1, 'Second', 1, '2018-05-21 07:53:16', '2018-06-05 10:53:46', NULL),
(3, 'ဦးကိုကိုကျော်', 'U Ko Ko Kyaw', 11, 1, 1, 'Third', 1, '2018-05-23 07:24:52', '2018-06-05 21:29:51', NULL),
(4, 'အောင်လွင်ဦး', 'U Aung Lwin Oo', 5, 4, 5, 'Fourth', 1, '2018-05-29 04:03:37', '2018-06-05 11:13:11', NULL),
(5, 'U Testing', 'U Testing', 41, 1, 1, 'asdf', 1, '2018-06-06 04:52:27', '2018-06-05 22:24:11', NULL),
(6, 'U Testing Two', 'U Testing Two', 41, 1, 1, 'U Testing Two', 1, '2018-06-06 06:27:40', '2018-06-05 23:57:49', NULL);

-- --------------------------------------------------------

--
-- Stand-in structure for view `total_categories`
--
CREATE TABLE `total_categories` (
`count_category` bigint(21)
,`count_quarter` bigint(21)
,`housing_category_id` int(11)
,`quarter_id` int(11)
,`ministry_id` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `total_results`
--
CREATE TABLE `total_results` (
`count_category` bigint(21)
,`count_quarter` bigint(21)
,`housing_category_id` int(11)
,`quarter_id` int(11)
,`ministry_id` int(11)
);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@gmail.com', '$2y$10$qLHxnUALOEfG7MDtJ/aAkuMd/3Fo7bmyjT0ebzqgVEUcrTZ3m3Cv6', NULL, '2018-02-24 23:59:39', '2018-02-24 23:59:39');

-- --------------------------------------------------------

--
-- Structure for view `total_categories`
--
DROP TABLE IF EXISTS `total_categories`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `total_categories`  AS  select count(`allocates`.`housing_category_id`) AS `count_category`,count(`allocates`.`quarter_id`) AS `count_quarter`,`allocates`.`housing_category_id` AS `housing_category_id`,`allocates`.`quarter_id` AS `quarter_id`,`allocates`.`ministry_id` AS `ministry_id` from `allocates` where ((`allocates`.`staff_id` is not null) and (`allocates`.`ministry_id` is not null)) group by `allocates`.`ministry_id`,`allocates`.`housing_category_id` ;

-- --------------------------------------------------------

--
-- Structure for view `total_results`
--
DROP TABLE IF EXISTS `total_results`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `total_results`  AS  select count(`allocates`.`housing_category_id`) AS `count_category`,count(`allocates`.`quarter_id`) AS `count_quarter`,`allocates`.`housing_category_id` AS `housing_category_id`,`allocates`.`quarter_id` AS `quarter_id`,`allocates`.`ministry_id` AS `ministry_id` from `allocates` where ((`allocates`.`staff_id` is not null) and (`allocates`.`ministry_id` is not null)) group by `allocates`.`ministry_id`,`allocates`.`housing_category_id`,`allocates`.`quarter_id` ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `allocates`
--
ALTER TABLE `allocates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `buildings`
--
ALTER TABLE `buildings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `housing_categories`
--
ALTER TABLE `housing_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `housing_ones`
--
ALTER TABLE `housing_ones`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ministries`
--
ALTER TABLE `ministries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `old_allocates`
--
ALTER TABLE `old_allocates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `positions`
--
ALTER TABLE `positions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quarters`
--
ALTER TABLE `quarters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `staffs`
--
ALTER TABLE `staffs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `allocates`
--
ALTER TABLE `allocates`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `buildings`
--
ALTER TABLE `buildings`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `housing_categories`
--
ALTER TABLE `housing_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `housing_ones`
--
ALTER TABLE `housing_ones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `ministries`
--
ALTER TABLE `ministries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `old_allocates`
--
ALTER TABLE `old_allocates`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `positions`
--
ALTER TABLE `positions`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;
--
-- AUTO_INCREMENT for table `quarters`
--
ALTER TABLE `quarters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `staffs`
--
ALTER TABLE `staffs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
