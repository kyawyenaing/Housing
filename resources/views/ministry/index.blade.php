@extends('layouts.master')
@section('content')
	<div class="main-content">
		<div class="main-content-inner">
	        <div class="page-content">
				<div class="row">
					<div class="panel panel-default">
						<div class="panel-heading">
							<div class="title text-info text-center">
								ဝန်ကြီးဌာနများစာရင်း
							</div>
							<a class="btn btn-primary" href="{{url('/ministry/create')}}">
								<i class="fa fa-fw fa-plus"></i>  
								ဝန်ကြီးဌာန အသစ်တစ်ခု ထည့်သွင်းရန်
							</a>
							@if(session ('success'))
							<div id="successMessage" class="alert alert-success text-center col-md-4 pull-right">
								<button type="button" class="close" data-dismiss="alert">×</button>
								{{  session('success') }}
							</div>
							@endif
						</div>
						<div class="panel-body">
							<div class="col-xs-12 table-responsive ">
								<table class="table table-bordered" id="users">
								    <thead>
								        <tr>
								            <th>စဉ်</th>
								            <th>ဝန်ကြီးဌာနအမည်</th>
								            <th>ဝန်ကြီးဌာနအမည်(English)</th>
								            <th>အသေးစိတ်ဖော်ပြချက်(သို့)မှတ်ချက်များ</th>
								            <th>စီမံခန့်ခွဲရန်</th>
								        </tr>
								    </thead>
								</table>
							</div>
						</div>
						<div class="panel-footer">
							<a class="btn btn-danger" href="#" onclick="goBack()">
							     <i class="ace-icon fa fa-undo bigger-110"></i>ရှေ့စာမျက်နှာသို့
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
@section('js')
	<script>
		$(document).ready(function() {
		    oTable = $('#users').DataTable({
		        "processing": true,
		        "serverSide": true,
		        "ajax": "{{ route('datatable.ministry') }}",
		        "columns": [
		            {data: 'DT_Row_Index', orderable: false, searchable: false},
		            { data: 'name', name: 'name' },
		            { data: 'name_en', name: 'name_en' },
		            { data: 'description', name: 'description' },
		            {data: 'action', name: 'action',searchable: false,orderable:false}
		        ]
		    });
		});
	</script>
@endsection