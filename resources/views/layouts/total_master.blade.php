<!DOCTYPE html>
<html>
<head>

  <title>NPTDC-Housing</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

  @yield('css')
  <!-- Scripts -->
    <script>
      window.Laravel = {!! json_encode([
        'csrfToken' => csrf_token(),
        ]) !!};
    </script>
    
    <link rel="shortcut icon" type="icon/image" href="{{url('/assets/images/fav.png')}}">
    <!-- bootstrap & fontawesome -->
    <link rel="stylesheet" href="{{url('/assets/css/bootstrap.min.css')}}" />
    <link rel="stylesheet" href="{{url('/assets/css/bootstrap-select.css')}}" />
    <link rel="stylesheet" href="{{url('/assets/font-awesome/4.5.0/css/font-awesome.min.css')}}" />
    <!-- text fonts -->
    <link rel="stylesheet" href="{{url('/assets/css/fonts.googleapis.com.css')}}" />

    <!-- ace styles -->
    <link rel="stylesheet" href="{{url('/assets/css/ace.min.css')}}" class="ace-main-stylesheet" id="main-ace-style" />

    <link rel="stylesheet" href="{{url('/assets/css/styles.css')}}">
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
    <!-- datatable -->
    <link rel="stylesheet" href="{{url('/assets/css/jquery.dataTables.min.css')}}" />
    <link rel="stylesheet" href="{{url('/assets/css/dataTables.bootstrap.min.css')}}" />
    <!-- datepicker -->
    <link rel="stylesheet" href="{{url('assets/css/bootstrap-datepicker3.min.css')}}" type="text/css">
  </head>
  <!-- FOR BODY -->
  <body class="no-skin">

   @include('partials.header')
   <div class="main-container ace-save-state" id="main-container">
     @yield('content')
     <!-- Modal Dialog -->
     <div class="modal" id="confirmDelete" role="dialog" aria-labelledby="confirmDeleteLabel" aria-hidden="true">
       <div class="modal-dialog">
         <div class="modal-content">
           <div class="modal-header">
             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
             <h4 class="modal-title">အသိပေးချက်</h4>
           </div>
           <div class="modal-body">
             <b class="text-danger">ယခုအိမ်ခန်းကို ဖျက်ပစ်မည် သေချာပါသလား? </b>
           </div>
           <div class="modal-footer">
             <button type="button" class="btn btn-default" data-dismiss="modal">မဖျက်ပါ</button>
             <button type="button" class="btn btn-danger" id="confirm">ဖျက်ပစ်မည် သေချာပါသည်</button>
           </div>
         </div>
       </div>
     </div>
     <!--  -->
      <!-- InitAllocate Modal Dialog -->
      <div class="modal" id="confirmAllocate" role="dialog" aria-labelledby="confirmAllocateLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title">အသိပေးချက်</h4>
            </div>
            <div class="modal-body">
              <ul class="list-none">
                <b class="text-danger">
                  !!<b class="text-success">အခန်းသစ်များဆောက်ပြီး ဝန်ထမ်းများကို နေရာချပေးထားရန် </b>သေချာမှဆက်လက်လုပ်ဆောင်ပါရန်
                </b>
              </ul>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal">မလုပ်ဆောင်ပါ</button>
              <button type="button" class="btn btn-primary" id="confirm">ဆက်လက်လုပ်ဆောင်မည်</button>
            </div>
          </div>
        </div>
      </div>
     <!--  -->
     <div class="modal" id="removeStaff" role="dialog" aria-labelledby="remarkLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title">အသိပေးချက်</h4>
            </div>
            <div class="modal-body">
              <ul class="list-none">
                <b class="text-danger">
                  !!ယခု အိမ်ခန်းတွင် ဝန်ထမ်းနေရာကို နေရာချထားပေးရန် သေချာမှ ဆက်လက်လုပ်ဆောင်ပါရန်
                </b>
              </ul>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal">မလုပ်ဆောင်ပါ</button>
              <button type="button" class="btn btn-primary" id="confirm">ဆက်လက်လုပ်ဆောင်မည်</button>
            </div>
          </div>
        </div>
      </div>
     @include('partials.footer')
   </div>
   <script type="text/javascript" src="{{url('/assets/js/jquery-2.1.4.min.js')}}"></script>
   <!-- datatable -->
   <script type="text/javascript" src="{{url('/assets/js/jquery.dataTables.min.js')}}"></script>
    <!-- bootstrap -->
    <script type="text/javascript" src="{{url('/assets/js/bootstrap.min.js')}}"></script>
   <script type="text/javascript" src="{{url('/assets/js/bootstrap-select.js')}}"></script>
   <script type="text/javascript" src="{{url('assets/js/bootstrap-datepicker.js')}}"></script>
     <script type="text/javascript" src="{{url('/assets/js/ace.min.js')}}"></script>
   <script src="{{url('/assets/js/nptdchr.js')}}"></script>
   <script src="{{url('/assets/js/multiple_form.js')}}"></script>
   
    <script type="text/javascript">
        $(document).ready(function(){
          $('#confirmDelete').on('show.bs.modal', function (e) {
                $message = $(e.relatedTarget).attr('data-message');
                $(this).find('.modal-body p').text($message);
                $title = $(e.relatedTarget).attr('data-title');
                $(this).find('.modal-title').text($title);

                var form = $(e.relatedTarget).closest('form');
                $(this).find('.modal-footer #confirm').data('form', form);
            });  

            $('#confirmDelete').find('.modal-footer #confirm').on('click', function(){
                $(this).data('form').submit();
            });

            // 
            $('#confirmAllocate').on('show.bs.modal', function (e) {
                  $message = $(e.relatedTarget).attr('data-message');
                  $(this).find('.modal-body p').text($message);
                  $title = $(e.relatedTarget).attr('data-title');
                  $(this).find('.modal-title').text($title);

                  var form = $(e.relatedTarget).closest('form');
                  $(this).find('.modal-footer #confirm').data('form', form);
              });  

              $('#confirmAllocate').find('.modal-footer #confirm').on('click', function(){
                  $(this).data('form').submit();
              });
              $('#removeStaff').on('show.bs.modal', function (e) {
                  $message = $(e.relatedTarget).attr('data-message');
                  $(this).find('.modal-body p').text($message);
                  $title = $(e.relatedTarget).attr('data-title');
                  $(this).find('.modal-title').text($title);

                  var form = $(e.relatedTarget).closest('form');
                  $(this).find('.modal-footer #confirm').data('form', form);
              });  

              $('#removeStaff').find('.modal-footer #confirm').on('click', function(){
                  $(this).data('form').submit();
              });
            // 
            function printDiv(divName) {
              var printContents = document.getElementById(divName).innerHTML;
              var originalContents = document.body.innerHTML;

              document.body.innerHTML = printContents;

              window.print();

              document.body.innerHTML = originalContents;
            }
          $('.selectpicker').selectpicker();
          $("#living_date").datepicker({
            // autoclose: true,
            // todayHighlight: true
          });
        });
    </script>
    @yield('js')
   <!-- @stack('scripts') -->
 </body>
 </html>