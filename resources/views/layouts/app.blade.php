<!DOCTYPE html>
<html>
<head>
  <title>NPTDC-HR</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
  @yield('scc')
  <!-- Scripts -->
  <script>
    window.Laravel = {!! json_encode([
      'csrfToken' => csrf_token(),
      ]) !!};
    </script>
    
    <link rel="shortcut icon" type="icon/image" href="{{url('/assets/images/fav.png')}}">
    <!-- bootstrap & fontawesome -->
    <link rel="stylesheet" href="{{url('/assets/css/bootstrap.min.css')}}" />
    <link rel="stylesheet" href="{{url('/assets/font-awesome/4.5.0/css/font-awesome.min.css')}}" />

    <!-- text fonts -->
    <link rel="stylesheet" href="{{url('/assets/css/fonts.googleapis.com.css')}}" />
    <!-- ace styles -->
    <link rel="stylesheet" href="{{url('/assets/css/ace.min.css')}}" class="ace-main-stylesheet" id="main-ace-style" />

    <link rel="stylesheet" href="{{url('/assets/css/ace-skins.min.css')}}" />
    <link rel="stylesheet" href="{{url('/assets/css/ace-rtl.min.css')}}" />
    <link rel="stylesheet" href="{{url('/assets/css/styles.css')}}">

  </head>
  <body class="login-layout">

   @yield('content')
   
   <script type="text/javascript" src="{{url('/assets/js/jquery-2.1.4.min.js')}}"></script>

   <script  type="text/javascript" src="{{url('assets/js/bootstrap.min.js')}}"></script>

   @yield('js')
 </body>
 </html>