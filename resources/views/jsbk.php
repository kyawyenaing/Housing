@section('js')
    $(document).ready(function() {
        oTable = $('#users').DataTable({
            "processing": true,
            "serverSide": true,
            <!-- "ajax": "{{ route('datatable.getposts') }}", -->
            "ajax": {
                url: '{{ url("datatable/getposts") }}',
                data: function (d) {
                    d.user = $('select[name=user]').val();
                }
            },
            "columns": [

                { data: 'id', name: 'id' },
                { data: 'title', name: 'title' },
                { data: 'user_id', name: 'user_id' },
                { data: 'created_at', name: 'created_at' },
                { data: 'updated_at', name: 'updated_at' }

            ]
        });
    });
    $('#user').on('change', function(e) {
        oTable.draw();
        e.preventDefault();
    });
@endsection