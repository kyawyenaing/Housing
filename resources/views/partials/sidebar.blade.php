<div id="sidebar" class="sidebar responsive ace-save-state">

	<ul class="nav nav-list">
		<li class="{{ Request::is('home*')?'active':''}}">				 
			<a href="{{ url('/home')}}">
				<i class="menu-icon fa fa-tachometer"></i>
				<span class="menu-text"> Dashboard </span>
			</a>
			<b class="arrow"></b>
		</li>
		<li class="{{ Request::is('user*')?'active':''}}">				 
			<a href="{{ url('/user')}}">
				<i class="menu-icon fa fa-users"></i>
				<span class="menu-text"> Admin </span>
			</a>
			<b class="arrow"></b>
		</li>
		<!--  -->
		<li class="{{ Request::is('married*')?'active':''}}">
			<a href="#" class="dropdown-toggle">
			    <!-- <i class="menu-icon fa fa-list"></i> -->
			    <span class="menu-text">အိမ်ထောင်သည်အိမ်ရာများ</span>
			    <b class="arrow fa fa-angle-down"></b>
			</a>
		    <b class="arrow"></b>
			<ul class="submenu">
				<li class="{{ Request::is('married/buildings*')?'active':''}}">				 
					<a href="{{ url('/married/buildings') }}">
						<i class="menu-icon fa fa-arrows-alt"></i>
						<span class="menu-text">အဆောက်အဦ(တိုက်)များ</span>
					</a>
					<b class="arrow"></b>
				</li>	
				<li class="{{ Request::is('married/allocated-buildings')?'active':''}}" >		
					<a href="{{ url('/married/allocated-buildings') }}">
						<i class="menu-icon fa fa-buildings"></i>
						<span class="menu-text">အဆောက်အဦအလိုက်အခန်းများ</span>
					</a>
					<b class="arrow"></b>
				</li>			  
			  <li class="{{ Request::is('married/allocated-ministries')?'active':''}}" >		
					<a href="{{ url('/married/allocated-ministries') }}">
						<i class="menu-icon fa fa-buildings"></i>
						<span class="menu-text">ဝန်ကြီးဌာနအလိုက် အိမ်ရာချပေးထားမှုစာရင်း</span>
					</a>
					<b class="arrow"></b>
				</li>
				<li class="{{ Request::is('married/allocated-quarters')?'active':''}}" >		
					<a href="{{ url('/married/allocated-quarters') }}">
						<i class="menu-icon fa fa-buildings"></i>
						<span class="menu-text">ရပ်ကွက်အလိုက် အိမ်ရာချပေးထားမှုစာရင်း</span>
					</a>
					<b class="arrow"></b>
				</li>
				<li class="{{ Request::is('married/allocated-total')?'active':''}}" >		
					<a href="{{ url('/married/allocated-total') }}">
						<i class="menu-icon fa fa-buildings"></i>
						<span class="menu-text">အနှစ်ချုပ်</span>
					</a>
					<b class="arrow"></b>
				</li>
			</ul>
		</li>
		<!--  -->
		<li class="">
			<a href="#" class="dropdown-toggle">
			    <!-- <i class="menu-icon fa fa-list"></i> -->
			    <span class="menu-text">အိမ်ရာဆိုင်ရာအချက်အလက်များ</span>
			    <b class="arrow fa fa-angle-down"></b>
			</a>
		    <b class="arrow"></b>
			<ul class="submenu">
				<li class="{{ Request::is('quarter*')?'active':''}}">				 
					<a href="{{ url('/quarter') }}">
						<i class="menu-icon fa fa-arrows-alt"></i>
						<span class="menu-text">ရပ်ကွက်များ</span>
					</a>
					<b class="arrow"></b>
				</li>
				<li class="{{ Request::is('housing_category*')?'active':''}}">			
					<a href="{{ url('/housing_category') }}">
						<i class="menu-icon fa fa-building"></i>
						<span class="menu-text">အိမ်ရာအမျိုးအစားများ</span>
					</a>
					<b class="arrow"></b>
				</li>
			</ul>
		</li>
		<!--  -->
		<!--  -->
		<li class="">
			<a href="#" class="dropdown-toggle">
			    <!-- <i class="menu-icon fa fa-list"></i> -->
			    <span class="menu-text">ဝန်ထမ်းဆိုင်ရာအချက်အလက်များ</span>
			    <b class="arrow fa fa-angle-down"></b>
			</a>
		    <b class="arrow"></b>
			<ul class="submenu">
				<li class="{{ Request::is('ministry*')?'active':''}}">				 
				<a href="{{ url('/ministry') }}">
					<i class="menu-icon fa fa-institution"></i>
					<span class="menu-text">ဝန်ကြီးဌာနများ</span>
				</a>
				<b class="arrow"></b>
				</li> 
				<li class="{{ Request::is('department*')?'active':''}}">				 
					<a href="{{ url('/department') }}">
						<i class="menu-icon fa fa-tachometer"></i>
						<span class="menu-text">ဦးစီးဌာနများ</span>
					</a>
					<b class="arrow"></b>
				</li> 

				<li class="{{ Request::is('position*')?'active':''}}">				 
					<a href="{{ url('/position') }}">
						<i class="menu-icon fa fa-random"></i>
						<span class="menu-text">ရာထူးများ</span>
					</a>
					<b class="arrow"></b>
				</li> 
				<li class="{{ Request::is('rank*')?'active':''}}">				 
					<a href="{{ url('/rank') }}">
						<i class="menu-icon fa fa-random"></i>
						<span class="menu-text">Rank</span>
					</a>
					<b class="arrow"></b>
				</li> 
				<li class="{{ Request::is('staff*')?'active':''}}">				 
					<a href="{{ url('/staff') }}">
						<i class="menu-icon fa fa-user"></i>
						<span class="menu-text">ဝန်ထမ်းများ</span>
					</a>
					<b class="arrow"></b>
				</li>
			</ul>
		</li>
		<!--  -->
		<li class="{{ Request::is('living*')?'active':''}}">				 
			<a href="{{ url('living') }}">
				<span class="menu-text">အိမ်ရာချထားမှု စာရင်း</span>
			</a>
			<b class="arrow"></b>
		</li>
		<!--  -->
		
	</ul>
</div>
