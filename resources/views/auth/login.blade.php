@extends('layouts.app')

@section('content')

<div class="main-container">
    <div class="main-content">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1">
                <div class="login-container">
                    <div class="navbar-fixed-top align-right">
                        <br>

                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                 
                    </div><br>

                    <div class="space-6"></div>

                    <div class="position-relative">
                        <div id="login-box" class="login-box visible widget-box no-border">
                            <div class="widget-body">
                                <div class="widget-main">          
                                 <div class="center login_heading">
                                 <h3 style="line-height:1.5;">
                                    <img src="{{url('/assets/images/logo.png')}}" width="60px" height="40px">
                                        <span class="white">နေပြည်တော် စည်ပင်သာယာရေးကော်မတီ</span>
                                    </h3>
                                    <h5 class="nptdc" id="id-company-text">NPTDC Housing </h5>
                                </div>

                                <div class="space-6"></div>

                                <form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">
                                    {{ csrf_field() }}                   

                                    <fieldset>

                                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                            <label for="email" class="block clearfix">
                                                <span class="block input-icon input-icon-right">
                                                    <input type="email" class="form-control" placeholder="Email Address" name="email" value="{{ old('email') }}" />
                                                    <i class="ace-icon fa fa-envelope"></i>
                                                </span>
                                                @if ($errors->has('email'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                                @endif

                                            </label>
                                        </div>


                                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                            <label for="password" class="block clearfix">
                                                <span class="block input-icon input-icon-right">
                                                    <input type="password" class="form-control" placeholder="Password" name="password" />
                                                    <i class="ace-icon fa fa-lock"></i>
                                                </span>
                                                @if ($errors->has('password'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                                @endif

                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label class="block clearfix">

                                                <div class="block input-icon input-icon-right">                        
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                                            <div class="blue">Remember Me</div>
                                                        </label>
                                                    </div>

                                                </div>
                                            </label>
                                        </div>

                                        <div class="form-group">
                                            <button type="submit" class="btn-block pull-right btn btn-sm btn-primary">
                                                <i class="ace-icon fa fa-key"></i>
                                                <span class="bigger-110">Login</span>
                                            </button>
                                        </div>

                                    </fieldset>

                                </form>
                            </div><!-- /.widget-main -->

                        </div><!-- /.widget-body -->
                    </div><!-- /.login-box -->


                </div><!-- /.position-relative -->

            </div>
        </div><!-- /.col -->
    </div><!-- /.row -->
</div><!-- /.main-content -->
@endsection
