@extends('layouts.master')
@section('content')
	<div class="main-content">
		<div class="main-content-inner">
			<div class="breadcrumbs ace-save-state" id="breadcrumbs">
				<ul class="breadcrumb">
					<li>
						<i class="ace-icon fa fa-home home-icon"></i>
						<a href="{{url('/user')}}">Home</a>
					</li>
					<li>
						<a href="#">Admin</a>
					</li>							
				</ul>
			</div>
			
			<div class="page-header">
				<a class="btn btn-primary btn-sm" href="{{url('/user/create')}}">
					<i class="fa fa-fw fa-plus"></i>  
					Add New Admin
				</a>
			</div>
	        <div class="page-content">
				<div class="row">
					<div class="col-xs-12 table-responsive ">
						<table class="table table-bordered" id="users">
						    <thead>
						        <tr>
						            <th>Id</th>
						            <th>Name</th>
						            <th>Created At</th>
						            <th>Updated At</th>
						            <th>Action</th>
						        </tr>
						    </thead>
						</table>
					</div>
				</div>
			</div>

		</div>
	</div>
@endsection
@section('js')
	<script>
		$(document).ready(function() {

		    oTable = $('#users').DataTable({
		        "processing": true,
		        "serverSide": true,
		        "ajax": "{{ route('datatable.user') }}",
		        "columns": [
		            { data: 'id', name: 'id' },
		            { data: 'name', name: 'name' },
		            { data: 'created_at', name: 'created_at' },
		            { data: 'updated_at', name: 'updated_at' },
		            {data: 'action', name: 'action',searchable: false}
		        ]
		    });

		});
	</script>
@endsection