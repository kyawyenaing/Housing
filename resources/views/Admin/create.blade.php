@extends ('layouts.master')

@section('content')

	<div class="main-content">
      <div class="main-content-inner">
      	<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="{{url('/user')}}">Home</a>
				</li>

				<li>
					<a href="#">Admin</a>
				</li>
		
			</ul><!-- /.breadcrumb -->
		</div>
		<div class="page-header">
			<h1>
				Create New Admin					
			</h1>
		</div><!-- /.page-header -->
		<div class="row">
			<div class="col-xs-12">
				<form class="form-horizontal" method="POST" action="{{url('/user')}}" enctype="multipart/form-data">
                     {{ csrf_field() }}

                  <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
						<label for="name" class="col-md-4 control-label">Name</label>

						<div class="col-md-6">						   
							<input id="name" type="text" class="form-control" name="name" value=""> 	
							@if ($errors->has('name'))					   
							<span class="help-block">
								<strong>{{ $errors->first('name') }}</strong>
							</span>
							@endif    
						</div>

					</div>

					<div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
						<label for="email" class="col-md-4 control-label">E-Mail Address</label>

						<div class="col-md-6">						   
							<input id="email" name="email" type="email" class="form-control" placeholder="example@gmail.com" value=""> 	   
							@if ($errors->has('email'))
							<span class="help-block">
								<strong>{{ $errors->first('email') }}</strong>
							</span>
							@endif         
						</div>
						
					</div>

					<div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
						<label for="Password" class="col-md-4 control-label">Password</label>
						<div class="col-md-6">						   
							<input id="Password" type="Password" class="form-control" name="password"> 	
							@if ($errors->has('password'))
							<span class="help-block">
								<strong>{{ $errors->first('password') }}</strong>
							</span>
							@endif
						</div>
					</div>

					
					<div class="form-group">
						<label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

						<div class="col-md-6">
							<input id="password-confirm" type="password" class="form-control" name="password_confirmation">
						</div>
					</div>
					

				   
			      <div class="form-group">
			              <div class="col-md-6 col-md-offset-4">
			                  <button type="submit" class="btn btn-success">
			                    <i class="ace-icon fa fa-check bigger-110"></i>
			                      Save
			                  </button>
			                  <a class="btn btn-danger" href="{{url('/user')}}">
			                  <i class="ace-icon fa fa-undo bigger-110"></i>Cancel
			                   </a>
			              </div>
			        </div>

                </form>
		    </div>
		</div>
      </div>
	</div>

@endsection

