@extends('layouts.master')
@section('content')
<div class="main-content">
	<!--  -->
    <div class="main-content-inner">
      	<!--  -->
		<div class="page-content">
			<!--  -->
	        <div class="row">
	        	<!--  -->
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="text-info">
							ဦးစီးဌာနအသစ်တစ်ခု ထည့်သွင်းခြင်း
						</h4>
					</div>
					<div class="panel-body">
						<div class="col-xs-12">				
			               <form class="form-horizontal" method="POST" action="{{url('/department')}}" enctype="multipart/form-data">
			               	   {{ csrf_field() }}

			               	    <div class="form-group {{ $errors->has('ministry_id') ? ' has-error' : '' }}">
			               	    	<label for="ministry_id" class="col-md-4 control-label">ဝန်ကြီးဌာန</label>
			               	    	<div class="col-md-6">						   
			               	    	<select id="ministry_id" name="ministry_id" class="form-control selectpicker" data-live-search="true" required>
			               	    			<option value="">ဝန်ထမ်း၏ ဝန်ကြီးဌာနကို ရွေးချယ်ပေးပါ</option>
			               	    			@foreach($ministries as $ministry)
			               	    			<option value="{{$ministry->id}}" {{old('ministry_id') == $ministry->id ? 'selected' : ''}}>{{$ministry->name}}</option>
			               	    			@endforeach
			               	    	</select>		
			               	    		@if ($errors->has('ministry_id'))					 
			               	    		<span class="help-block">
			               	    			<strong>{{ $errors->first('ministry_id') }}</strong>
			               	    		</span>
			               	    		@endif    
			               	    	</div>
			               	    </div>
			               	    <!--  -->
			               		 <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
									<label for="name" class="col-md-4 control-label">ဦးစီးဌာနအမည်(Myanmar)</label>
									<div class="col-md-6">						   
										<input type="text" class="form-control" name="name" value="{{ old('name') }}"> 	
										@if ($errors->has('name'))					   
										<span class="help-block">
											<strong>{{ $errors->first('name') }}</strong>
										</span>
										@endif    
									</div>
								</div>
								<!--  -->
								<div class="form-group {{ $errors->has('name_en') ? ' has-error' : '' }}">
									<label for="name" class="col-md-4 control-label">ဦးစီးဌာနအမည်(English)</label>
									<div class="col-md-6">						   
										<input type="text" class="form-control" name="name_en" value="{{ old('name_en') }}"> 	
										@if ($errors->has('name_en'))					   
										<span class="help-block">
											<strong>{{ $errors->first('name_en') }}</strong>
										</span>
										@endif    
									</div>
								</div>
								<!--  -->
								<div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
									<label for="name" class="col-md-4 control-label">အသေးစိတ်ဖော်ပြချက်(သို့)မှတ်ချက်များ</label>
									<div class="col-md-6">						   
										<textarea class="form-control" rows="5" name="description" id="description">{{old('description')}}</textarea>
										  @if ($errors->has('description'))					   
										<span class="help-block">
											<strong>{{ $errors->first('description') }}</strong>
										</span>
										@endif
									</div>
								</div>
								<!--  -->
			                    <div class="form-group">
					              <div class="col-md-6 col-md-offset-4">
					                  <button type="submit" class="btn btn-success">
					                    <i class="ace-icon fa fa-check bigger-110"></i>
					                      Save
					                  </button>
					                  <a class="btn btn-danger" href="{{url('/department')}}">
					                     <i class="ace-icon fa fa-undo bigger-110"></i>Cancel
					                  </a>
					              </div>
						        </div>
				             	<!--  -->
			               </form>
						</div>
					</div>
				</div>
			</div>
			<!--  -->
		</div>
		<!--  -->
	</div>
	<!--  -->
</div>
<!--  -->
@endsection