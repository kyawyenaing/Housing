@extends('layouts.master')
@section('content')
	<div class="main-content">
		<div class="main-content-inner">
	        <div class="page-content">
				<div class="row">
					<div class="panel panel-default">
						<div class="panel-heading">
							<div class="row">
								<div class="title text-info text-center">
									<span id="cat"></span>
									ရပ်ကွက်အလိုက် အိမ်ရာရရှိမှုစာရင်း
								</div>
							</div>
							<div class="row">
								<!--  -->
								<div class="col-md-4 col-md-4 text-center">
									<select name="quarter" id="quarter" class="form-control selectpicker" data-live-search="true">
									    <option value="" selected>ရပ်ကွက်ကိုရွေးပါ</option>
									    @foreach($quarters as $result)
									    <option value="{{$result->name}}">{{$result->name}}</option>
									    @endforeach
									</select>
								</div>
								<div class="col-md-4 col-md-4 text-center">
									<select name="category" id="category" class="form-control selectpicker" data-live-search="true">
									    <option value="" selected>အိမ်ရာအမျိုးအစားအလိုက် ကြည့်ရန်</option>
									    @foreach($housing_categories as $result)
									    <option value="{{$result->id}}">{{$result->name}}</option>
									    @endforeach
									</select>
								</div>
							</div>
						</div>
						<div class="panel-body">
							<div class="col-xs-12 table-responsive ">
								<table class="table_form table-bordered" id="allocated-quarters">
								    <thead>
								        <tr>
								            <th rowspan="2">စဉ်</th>
								            <th rowspan="2">ဝန်ကြီးဌာန</th>
								            <th rowspan="2">အဆောက်အဦအမှတ်</th>
								            <th rowspan="2">အခန်းအမှတ်</th>
								            <th colspan="3" class="text-center">နေထိုင်သူ</th>
								            <th rowspan="2">မှတ်ချက်</th>			
								        </tr>
								        <tr>
								        	<th class="text-center">အမည်</th>
								        	<th class="text-center">ရာထူး</th>
								        	<th class="text-center">ဦးစီးဌာန</th>
								        </tr>
								    </thead>
								</table>
							</div>
						</div>
						<div class="panel-footer">
							<a class="btn btn-danger" href="#" onclick="goBack()">
							     <i class="ace-icon fa fa-undo bigger-110"></i>ရှေ့စာမျက်နှာသို့
							</a>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
@endsection
@section('js')
	<script>
			$(document).ready(function() {
			    oTable = $('#allocated-quarters').DataTable({
			        "processing": true,
			        "serverSide": true,
			        "ajax" :{
		    			        url: '{{ route("datatable.allocated-quarters") }}',
		    			        data: function (d) {
		    			            d.quarter = $('select[name=quarter]').val();
		    			            d.category = $('select[name=category]').val();
		    			        }
		    			    },
				    drawCallback: function(settings){
					       var api = this.api();
					       var cat = $("select[name=quarter]").val();
					       $("#cat").html(cat);
					    },
			        "columns": [
			            { data: 'DT_Row_Index', orderable: false, searchable: false},
			            { data: 'ministry.name', name: 'ministry.name', },
			            { data: 'building.building_no', name: 'building.building_no',orderable: false },
			            { data: 'room_no', name: 'room_no' },
			            { data: 'staff.name', name: 'staff.name'},
			            { data: 'position_name', name: 'position_name',orderable: false },
			            { data: 'department_name', name:'department_name',orderable: false, searchable: false},
			            { data: 'remark', name:'remark',orderable: false, searchable: false},
			        ]
			    });
			    $('#quarter').on('change', function(e) {
			        oTable.draw();
			        e.preventDefault();
			    });
			    $('#category').on('change', function(e) {
			        oTable.draw();
			        e.preventDefault();
			    });

			});
	</script>
@endsection