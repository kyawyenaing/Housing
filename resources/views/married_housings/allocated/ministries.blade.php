@extends('layouts.master')
@section('content')
	<div class="main-content">
		<div class="main-content-inner">
	        <div class="page-content">
				<div class="row">
					<div class="panel panel-default">
						<div class="panel-heading">
							<div class="row">
								<div class="title text-info text-center">
									<span id="cat"></span>ဝန်ကြီးဌာန အိမ်ရာရရှိမှုစာရင်း
								</div>
							</div>
							<div class="row">
								<!--  -->
								<div class="col-md-4 col-md-4 text-center">
									<select name="ministry" id="ministry" class="form-control selectpicker" data-live-search="true">
									    <option value="" selected>ဝန်ကြီးဌာနကိုရွေးပါ</option>
									    @foreach($ministries as $result)
									    <option value="{{$result->name}}">{{$result->name}}</option>
									    @endforeach
									</select>
								</div>
								@if(session ('success'))
								<div id="successMessage" class="alert alert-success text-center col-md-4 pull-right">
									<button type="button" class="close" data-dismiss="alert">×</button>
									{{  session('success') }}
								</div>
								@endif
							</div>
						</div>
						<div class="panel-body">
							<div class="col-xs-12 table-responsive ">
								<table class="table table-bordered" id="allocated-ministries">
								    <thead>
								        <tr>
								            <th>စဉ်</th>
								            <th>အိမ်ရာအမျိုးအစား</th>
								            <th>ရပ်ကွက်အမည်</th>
								            <th>အဆောက်အဦအမှတ်</th>			
								            <th>အခန်းအရေအတွက်</th>
								            <th>အခန်းအမှတ်</th>
								            <th>ministry</th>
								            <th>မှတ်ချက်</th>
								            <th>စီမံခန့်ခွဲရန်</th>
								        </tr>
								    </thead>
								</table>
							</div>
						</div>
						<div class="panel-footer">
							<a class="btn btn-danger" href="#" onclick="goBack()">
							     <i class="ace-icon fa fa-undo bigger-110"></i>ရှေ့စာမျက်နှာသို့
							</a>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
@endsection
@section('js')
	<script>
			$(document).ready(function() {
			    oTable = $('#allocated-ministries').DataTable({
			        "processing": true,
			        "serverSide": true,
			        "ajax" :{
		    			        url: '{{ route("datatable.allocated-ministries") }}',
		    			        data: function (d) {
		    			            d.ministry = $('select[name=ministry]').val();
		    			        }
		    			    },
				    drawCallback: function(settings){
					       var api = this.api();
					       var cat = $("select[name=ministry]").val();
					       $("#cat").html(cat);
					    },
			        "columns": [
			            {data: 'DT_Row_Index', orderable: false, searchable: false},
			            { data: 'housing_category.name', name: 'housing_category.name' },
			            { data: 'quarter', name: 'quarter' },
			            { data: 'building.building_no', name: 'building.building_no',orderable: false },
			            { data: 'count_room', name:'count_room'},
			            { data: 'room_no', name:'room_no'},
			            { data: 'ministry.name', name:'ministry.name'},
			            { data: 'remark_by_ministry', name:'remark_by_ministry'},
			            {data: 'action', name: 'action',searchable: false, orderable: false}
			        ]
			    });
			    $('#ministry').on('change', function(e) {
			        oTable.draw();
			        e.preventDefault();
			    });
			});
	</script>
@endsection