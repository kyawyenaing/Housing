@extends('layouts.master')
@section('content')
	<div class="main-content">
		<div class="main-content-inner">
	        <div class="page-content">
				<div class="row">
					<div class="panel panel-default">
						<div class="panel-heading">
							<div class="row">
								<div class="title text-info text-center">
									<span id="cat"></span>အဆောက်အဦများနှင့်အခန်းများစာရင်း
								</div>
							</div>
							<div class="row">
								<!--  -->
								<div class="col-md-4 col-md-4 text-center">
									<select name="building" id="building" class="form-control selectpicker" data-live-search="true">
									    <option value="" selected>အဆောက်အဦအလိုက်ကြည့်ရန်</option>
									    @foreach($buildings as $result)
									    <option value="{{$result->building_no}}">{{$result->building_no}}({{$result->housing_category->name}})[{{$result->quarter->name}}]</option>
									    @endforeach
									</select>
								</div>
								<div class="col-md-4 col-md-4 text-center">
									<select name="status" id="status" class="form-control selectpicker" data-live-search="true">
									    <option value="" selected>နေရာချပေးမှုအလိုက်ကြည့်ရန်</option>
									    <option value="1">နေရာချထားပြီးသောအခန်းများ</option>
									    <option value="2">အခန်းလွတ်များ</option>
									    
									</select>
								</div>
								@if(session ('success'))
								<div id="successMessage" class="alert alert-success text-center col-md-4 pull-right">
									<button type="button" class="close" data-dismiss="alert">×</button>
									{{  session('success') }}
								</div>
								@endif
							</div>
						</div>
						<div class="panel-body">
							<div class="col-xs-12 table-responsive ">
								<table class="table_form table-bordered" id="allocated-buildings">
								    <thead>
								        <tr>
								            <th rowspan="2">စဉ်</th>			     
								            <th rowspan="2">အဆောက်အဦအမှတ်</th>
								            <th rowspan="2">အခန်းအမှတ်</th>
								            <th colspan="4" class="text-center">နေထိုင်သူ</th>
								            <th rowspan="2">မှတ်ချက်</th>	
								            <th rowspan="2">စီမံခန့်ခွဲရန်</th>		
								        </tr>
								        <tr>
								        	<th class="text-center">အမည်</th>
								        	<th class="text-center">ရာထူး</th>
								        	<th class="text-center">ဦးစီးဌာန</th>
								        	<th class="text-center">ဝန်ကြီးဌာန</th>
								        </tr>
								    </thead>
								</table>
							</div>
						</div>
						<div class="panel-footer">
							<a class="btn btn-danger" href="#" onclick="goBack()">
							     <i class="ace-icon fa fa-undo bigger-110"></i>ရှေ့စာမျက်နှာသို့
							</a>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
@endsection
@section('js')
	<script>
			$(document).ready(function() {
			    oTable = $('#allocated-buildings').DataTable({
			        "processing": true,
			        "serverSide": true,
			        "ajax" :{
		    			        url: '{{ route("datatable.allocated-buildings") }}',
		    			        data: function (d) {
		    			            d.building = $('select[name=building]').val();
		    			            d.status = $('select[name=status]').val();
		    			        }
		    			    },
				    drawCallback: function(settings){
					       var api = this.api();
					       var cat = $("select[name=building]").val();
					       $("#cat").html(cat);
					    },
			        "columns": [
			            { data: 'DT_Row_Index', orderable: false, searchable: false},			            
			            { data: 'building.building_no', name: 'building.building_no' },
			            { data: 'room_no', name: 'room_no' },
			            { data: 'staff_name', name: 'staff_name',orderable: false, searchable: false},
			            { data: 'position_name', name: 'position_name',orderable: false },
			            { data: 'department_name', name:'department_name',orderable: false, searchable: false},
			            { data: 'ministry_name', name: 'ministry_name',orderable: false,searchable: false },
			            { data: 'remark', name:'remark',orderable: false, searchable: false},
			            { data: 'action', name:'action',orderable: false,searchable: false},
			        ]
			    });
			    $('#building').on('change', function(e) {
			        oTable.draw();
			        e.preventDefault();
			    });
			    $('#status').on('change', function(e) {
			        oTable.draw();
			        e.preventDefault();
			    });

			});
	</script>
@endsection