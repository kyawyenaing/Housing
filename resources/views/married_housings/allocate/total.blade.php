@extends('layouts.total_master')
@section('content')
<div class="main-content">
	<!--  -->
	<div class="main-content-inner">
		<!--  -->
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="{{url('/home')}}">Home</a>
				</li>
				<li>
					<b class="text-info">

					</b>
				</li>							
			</ul>
		</div>
		<!--  -->
		<div class="page-header">
			<!--  -->
			<div class="col-md-4 col-sm-4 pull-right">
				<button class="btn btn-primary btn-sm btn-bold" onclick="printDiv('print_area')" >
					<i class="ace-icon fa fa-print bigger-130 blue"></i>Print
				</button>
			</div>
			<!--  -->
		</div>
		<!--  -->
        <div class="page-content">
        	<!--  -->
        	<div class="row">
        		<div class="col-xs-12">
        			<b class="text-center pull-left" id="total"></b>
        		</div>
        	</div>
			<div class="row">
				<!--  -->
				<div class="col-xs-12 table-responsive">
					<table class="table table-bordered text-center" id="initial">
					    <thead>
					        <tr>
								<th class="center">စဉ်</th>					
								<th class="center">ဝန်ကြီးဌာန</th>
								<th class="center">ဝန်ကြီး</th>
								<th>ဒု-ဝန်ကြီး</th>
								<th class="center">Duplex</th>
								<th colspan="{{count($housing_four)+1}}" class="center">2U3S</th>
								<th colspan="{{count($housing_five)+1}}" class="center">4U4S(3B)</th>
								<th colspan="{{count($housing_six)+1}}" class="center">6U4S(3B)</th>
								<th colspan="{{count($housing_seven)+1}}" class="center">4U4S(2B)</th>
								<th colspan="{{count($housing_eight)+1}}" class="center">6U4S(2B)</th>
								<!-- all -->
								<th class="text-danger">စုစုပေါင်း</th>
					        </tr>
					    </thead>
					    <tbody>
					    	<tr>
					    		<th></th>
								<th></th>
								<!-- one -->
								<th></th>
								<!-- two -->
								<th></th>
								<!-- three -->
								<th></th>
								<!-- four -->
								@foreach($housing_four as $res)
									<th class="text-center">{{$res->quarter->short_name}}</th>
								@endforeach
									<th class="text-info text-center">ပေါင်း</th>
								<!-- five -->
								@foreach($housing_five as $res)
									<th class="text-center">{{$res->quarter->short_name}}</th>
								@endforeach
									<th class="text-info text-center">ပေါင်း</th>
								<!-- six -->
								@foreach($housing_six as $res)
									<th class="text-center">{{$res->quarter->short_name}}</th>
								@endforeach
									<th class="text-info text-center">ပေါင်း</th>
								<!-- seven -->
								@foreach($housing_seven as $res)
									<th class="text-center">{{$res->quarter->short_name}}</th>
								@endforeach
									<th class="text-info text-center">ပေါင်း</th>
								<!-- eight -->
								@foreach($housing_eight as $res)
									<th class="text-center">{{$res->quarter->short_name}}</th>
								@endforeach
									<th class="text-info text-center">ပေါင်း</th>

					    	</tr>
					    	@foreach($ministries as $ministry)
					    	<tr>
				    			<td>{{$ministry->id}}</td>
				    			<td>{{$ministry->name}}</td>
				    			<td id="one-{{$ministry->id}}" class="text-center">
				    			@foreach($results as $result)	
	    			    			@if($result->ministry_id ==$ministry->id && $result->housing_category_id ==1)
	    			    				{{$result->count_category}}
	    			    			@else
	    			    				{{''}}
	    			    			@endif 
	    			    		@endforeach
	    			    		</td>
	    			    		<td id="two-{{$ministry->id}}" class="text-center">
    			    			@foreach($results as $result)		
	    			    			@if($result->ministry_id ==$ministry->id && $result->housing_category_id ==2)
	    			    				{{$result->count_category}}
	    			    			@else 
	    			    				{{''}}
	    			    			@endif 
	    			    		@endforeach
	    			    		</td>
	    			    		<td id="three-{{$ministry->id}}" class="text-center">
    			    			@foreach($results as $result)	
	    			    			@if($result->ministry_id ==$ministry->id && $result->housing_category_id ==3)
	    			    				{{$result->count_category}}
	    			    			@else 
	    			    				{{''}}
	    			    			@endif 
	    			    		@endforeach
	    			    		</td>
	    			    		<!-- four -->
	    			    		<?php $count = count($housing_four); ?>
    			    			@foreach($quarters as $quarter)	    		
	    			    			@if($quarter->ministry_id ==$ministry->id && $quarter->housing_category_id ==4)	  	
	    			    				<td class="text-center">
	    			    					@if(isset($quarter->count_category))
	    			    						{{$quarter->count_category}}
	    			    					@endif
	    			    				</td>	
	    			    				<?php $count-- ;?>    			    	
	    			    			@endif
	    			    		@endforeach	
    			    			@for($i=0;$i <$count;$i++)
    			    				<td class="text-center">{{"No Housing"}}</td>
    			    			@endfor
	    			    		<?php $tot= 0; ?>
	    			    		@foreach($total_quarters as $total)	
	    			    			@if($total->ministry_id ==$ministry->id && $total->housing_category_id ==4)
	    			    				<td class="text-info" id="four-{{$ministry->id}}">{{$total->total_quarter}}</td>
	    			    			<?php $tot++;?>
	    			    			@endif 
	    			    		@endforeach	
    			    			@if($tot==0)
    			    				<td>{{0}}</td>
    			    			@endif
	    			    		<!-- five -->
	    			    		<?php $count = count($housing_five); ?>
    			    			@foreach($quarters as $quarter)	    			  
	    			    			@if($quarter->ministry_id ==$ministry->id && $quarter->housing_category_id == 5)	   	
	    			    				<td class="text-center">
	    			    					@if(isset($quarter->count_category))
	    			    						{{$quarter->count_category}}
	    			    					@endif
	    			    				</td>	
	    			    				<?php $count-- ;?>    			    	
	    			    			@endif
	    			    		@endforeach	
    			    			@for($i=0;$i <$count;$i++)
    			    				<td class="text-center">{{"No Housing"}}</td>
    			    			@endfor
	    			    		<?php $tot= 0; ?>
	    			    		@foreach($total_quarters as $total)	
	    			    			@if($total->ministry_id ==$ministry->id && $total->housing_category_id == 5)
	    			    				<td class="text-info text-center" id="five-{{$ministry->id}}">{{$total->total_quarter}}</td>
	    			    			<?php $tot++;?>
	    			    			@endif 
	    			    		@endforeach	
    			    			@if($tot==0)
    			    				<td class="text-center">{{0}}</td>
    			    			@endif
    			    			<!-- six -->
    			    			<?php $count = count($housing_six); ?>
    			    			@foreach($quarters as $quarter)	    			  
	    			    			@if($quarter->ministry_id ==$ministry->id && $quarter->housing_category_id == 6)	   	  
	    			    				<td class="text-center">
	    			    					@if(isset($quarter->count_category))
	    			    						{{$quarter->count_category}}
	    			    					@endif
	    			    				</td>	
	    			    				<?php $count-- ;?>    			    	
	    			    			@endif
	    			    		@endforeach	
    			    			@for($i=0;$i <$count;$i++)
    			    				<td class="text-center">{{"No Housing"}}</td>
    			    			@endfor
	    			    		<?php $tot= 0; ?>
	    			    		@foreach($total_quarters as $total)	
	    			    			@if($total->ministry_id ==$ministry->id && $total->housing_category_id == 6)
	    			    				<td class="text-info text-center" id="six-{{$ministry->id}}">{{$total->total_quarter}}</td>
	    			    			<?php $tot++;?>
	    			    			@endif 
	    			    		@endforeach	
    			    			@if($tot==0)
    			    				<td class="text-center">{{0}}</td>
    			    			@endif
    			    			<!-- seven -->
    			    			<?php $count = count($housing_seven); ?>
    			    			@foreach($quarters as $quarter)	    			  
	    			    			@if($quarter->ministry_id ==$ministry->id && $quarter->housing_category_id == 7)	   	  
	    			    				<td class="text-center">
	    			    					@if(isset($quarter->count_category))
	    			    						{{$quarter->count_category}}
	    			    					@endif
	    			    				</td>	
	    			    				<?php $count-- ;?>    			    	
	    			    			@endif
	    			    		@endforeach	
    			    			@for($i=0;$i <$count;$i++)
    			    				<td class="text-center">{{"No Housing"}}</td>
    			    			@endfor
	    			    		<?php $tot= 0; ?>
	    			    		@foreach($total_quarters as $total)	
	    			    			@if($total->ministry_id ==$ministry->id && $total->housing_category_id == 7)
	    			    				<td class="text-info text-center" id="seven-{{$ministry->id}}">{{$total->total_quarter}}</td>
	    			    			<?php $tot++;?>
	    			    			@endif 
	    			    		@endforeach	
    			    			@if($tot==0)
    			    				<td class="text-center">{{0}}</td>
    			    			@endif
    			    			<!-- eight -->
    			    			<?php $count = count($housing_eight); ?>
    			    			@foreach($quarters as $quarter)	    			  
	    			    			@if($quarter->ministry_id ==$ministry->id && $quarter->housing_category_id == 8)	   	  
	    			    				<td class="text-center">
	    			    					@if(isset($quarter->count_category))
	    			    						{{$quarter->count_category}}
	    			    					@endif
	    			    				</td>	
	    			    				<?php $count-- ;?>    			    	
	    			    			@endif
	    			    		@endforeach	
    			    			@for($i=0;$i <$count;$i++)
    			    				<td class="text-center">{{"No Housing"}}</td>
    			    			@endfor
	    			    		<?php $tot= 0; ?>
	    			    		@foreach($total_quarters as $total)	
	    			    			@if($total->ministry_id ==$ministry->id && $total->housing_category_id == 8)
	    			    				<td class="text-info text-center" id="eight-{{$ministry->id}}">{{$total->total_quarter}}</td>
	    			    			<?php $tot++;?>
	    			    			@endif 
	    			    		@endforeach	
    			    			@if($tot==0)
    			    				<td class="text-center">{{0}}</td>
    			    			@endif
    			    			<td id="all-{{$ministry->id}}"></td>
				    		</tr>
					    	@endforeach 			
					    </tbody>
					</table>
				</div>
				<!--  -->
			</div>
			<!--  -->
		</div>
		<!--  -->
	</div>
	<!--  -->
</div>
<!--  -->
@endsection('content')
@section('js')
<script>
	$(document).ready(function(){
		<?php foreach($ministries as $ministry):?>
			if($("#one-<?php echo$ministry->id;?>").text() >0) {
			var one = parseInt($("#one-<?php echo $ministry->id;?>").text());
		} else {
			var one = 0;
		}
		if($("#two-<?php echo $ministry->id;?>").text() >0) {
			var two = parseInt($("#two-<?php echo $ministry->id;?>").text());
		} else {
			var two = 0;
		}
		if($("#three-<?php echo $ministry->id;?>").text() >0) {
			var three = parseInt($("#three-<?php echo $ministry->id;?>").text());
		} else {
			var three = 0;
		}
		if($("#four-<?php echo $ministry->id;?>").text() >0) {
			var four = parseInt($("#four-<?php echo $ministry->id;?>").text());
		} else {
			var four = 0;
		}
		if($("#five-<?php echo $ministry->id;?>").text() >0) {
		var five = parseInt($("#five-<?php echo $ministry->id;?>").text());
		} else {
			var five = 0;
		}
		if($("#six-<?php echo $ministry->id;?>").text() >0) {
			var six = parseInt($("#six-<?php echo $ministry->id;?>").text());
		} else {
			var six = 0;
		}
		if($("#seven-<?php echo $ministry->id;?>").text() >0) {
			var seven = parseInt($("#seven-<?php echo $ministry->id;?>").text());
		} else {
			var seven = 0;
		}
		if($("#eight-<?php echo $ministry->id;?>").text() >0) {
			var eight = parseInt($("#eight-<?php echo $ministry->id;?>").text());
		} else {
			var eight = 0;
		}
		var result = one + two + three + four + five + six+ seven+ eight;
		$("#all-<?php echo $ministry->id;?>").text(result);
		<?php endforeach;?>
		
	});
</script>
@endsection