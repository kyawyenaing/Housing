@extends('layouts.master')
@section('content')
<div class="main-content">
	<!--  -->
    <div class="main-content-inner">
        <div class="page-content">
        	<div class="row">
	         	<!--  -->
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="text-info">
						({{$building->housing_category->name}})/ 
						<b class="text-danger">
							[ တိုက်အမှတ် {{$building->building_no}} ]
						</b> ရှိအခန်းသစ်များတွင် <b>{{$ministry->name}}</b>ဝန်ကြီးဌာနမှ ဝန်းထမ်းများကို နေရာချထားခြင်း
					</h4>
					</div>
					<div class="panel-body">
						<div class="col-xs-12">
			               <form class="form-horizontal" method="POST" action="{{url('/married/allocate')}}" enctype="multipart/form-data">
			               	   {{ csrf_field() }}
			               	   <input type="hidden" name="building_id" value="{{$building->id}}">
			               	   <input type="hidden" name="housing_category_id" value="{{$building->housing_category_id}}">
			               	   <input type="hidden" name="quarter_id" value="{{$building->quarter_id}}">
			               	   <input type="hidden" name="ministry_id" value="{{$ministry->id}}">
								<!--  -->
								<div class="form-group text-center">
									<button type="button" class="btn btn-success" id="multiform">ADD</button>
								</div>
								<!--  -->
							 	<div class="row">
							 		<div id="first_form" class="col-md-12">
							 		 	<div class="form-group col-md-3 text-center">
							 		 		<label for="room_no" class=" control-label">အခန်းအမှတ်</label>	   
							 				<input type="text" class="form-control" name="room_no[]" value="{{old('room_no')}}"  autofocus> 
							 				@if ($errors->has('room_no.*'))
							 				<span class="help-block">
							 					<strong>{{ $errors->first('room_no.*') }}
							 					</strong>
							 				</span>
							 				@endif	     		       
							 		 	</div>
							 		 	<div class="form-group col-md-6 text-center">
							 		 		<label for="staff_id" class="control-label">နေရာချထားပေးမည့်ဝန်ထမ်း</label>
							 				<select name="staff_id[]" class="form-control selectpicker" data-live-search="true" id="staff_id">
							 					<option value="">နေရာချထားပေးမည့်ဝန်ထမ်း</option>
							 					@foreach($staffs as $result)
							 					<option value="{{$result->id}}" {{old('result_id') == $result->id ? 'selected' : ''}}>
							 						{{$result->name}} ->( {{$result->position->name}})({{$result->department->name}}/{{$result->ministry->name}})
							 					</option>
							 					@endforeach
							 		        </select>
							 		        @if ($errors->has('staff_id.*'))
							 		        <span class="help-block">
							 		        	<strong>{{ $errors->first('staff_id.*') }}
							 		        	</strong>
							 		        </span>
							 		        @endif
							 		 	</div>
							 		 	<div class="form-group col-md-3 text-center">
							 		 		<label for="staff_id" class="control-label">Remark</label>
							 				<textarea name="remark" cols="25"></textarea>
							 		 	</div>
							 		</div>
						 		 	<div id="second_form" class="col-md-12">
				 			 		<!-- colone one -->
						 			</div>
						 			<div id="third_form" class="col-md-12">
				 			 		<!-- colone two -->
						 			</div>
						 			<div id="fourth_form" class="col-md-12">
				 			 		<!-- colone three -->
						 			</div>
						 			<div id="fifth_form" class="col-md-12">
				 			 		<!-- colone four -->
						 			</div>
						 			<div id="sixth_form" class="col-md-12">
				 			 		<!-- colone five -->
						 			</div>
							 	</div>
							 	<!--  -->
			                    <div class="form-group">
					              <div class="col-md-6 col-md-offset-5">
					                  <button type="submit" class="btn btn-success">
					                    <i class="ace-icon fa fa-check bigger-110"></i>
					                      Save
					                  </button>
					                  <a class="btn btn-danger" href="#" onclick="goBack()">
					                     <i class="ace-icon fa fa-undo bigger-110"></i>Cancel
					                  </a>
					              </div>
						        </div>
						        <!--  -->               	
			               </form>
						</div>
					</div>
					<!-- here is clonable forms -->
					<div id="clone_one">
	 		 		 	<div class="form-group col-md-3 text-center">
	 		 		 		<label for="room_no" class=" control-label">အခန်းအမှတ်</label>	   
	 		 				<input type="text" class="form-control" name="room_no[]" value="{{old('room_no')}}" autofocus> 	     		       
	 		 		 	</div>
	 		 		 	<div class="form-group col-md-6 text-center">
	 		 		 		<label for="staff_id" class="control-label">နေရာချထားပေးမည့်ဝန်ထမ်း</label>
	 		 				<select name="staff_id[]" class="form-control selectpicker" data-live-search="true" id="staff_id">
	 		 					<option value="">နေရာချထားပေးမည့်ဝန်ထမ်း</option>
	 		 					@foreach($staffs as $result)
	 		 					<option value="{{$result->id}}" {{old('result_id') == $result->id ? 'selected' : ''}}>
	 		 						{{$result->name}} ->( {{$result->position->name}})({{$result->department->name}}/{{$result->ministry->name}})
	 		 					</option>
	 		 					@endforeach
	 		 		        </select>
	 		 		 	</div>
	 		 		 	<div class="form-group col-md-3 text-center">
	 		 		 		<label for="staff_id" class="control-label">Remark</label>
	 		 				<textarea name="remark" cols="25"></textarea>
	 		 		 	</div>
	 			 	</div>
	 			 	<div id="clone_two">
			 		 	<div class="form-group col-md-3 text-center">
			 		 		<label for="room_no" class=" control-label">အခန်းအမှတ်</label>	   
			 				<input type="text" class="form-control" name="room_no[]" value="{{old('room_no')}}" autofocus> 	     		       
			 		 	</div>
			 		 	<div class="form-group col-md-6 text-center">
			 		 		<label for="staff_id" class="control-label">နေရာချထားပေးမည့်ဝန်ထမ်း</label>
			 				<select name="staff_id[]" class="form-control selectpicker" data-live-search="true" id="staff_id">
			 					<option value="">နေရာချထားပေးမည့်ဝန်ထမ်း</option>
			 					@foreach($staffs as $result)
			 					<option value="{{$result->id}}" {{old('result_id') == $result->id ? 'selected' : ''}}>
			 						{{$result->name}} ->( {{$result->position->name}})({{$result->department->name}}/{{$result->ministry->name}})
			 					</option>
			 					@endforeach
			 		        </select>
			 		 	</div>
			 		 	<div class="form-group col-md-3 text-center">
			 		 		<label for="staff_id" class="control-label">Remark</label>
			 				<textarea name="remark" cols="25"></textarea>
			 		 	</div>
				 	</div>
				 	<div id="clone_three">
			 		 	<div class="form-group col-md-3 text-center">
			 		 		<label for="room_no" class=" control-label">အခန်းအမှတ်</label>	   
			 				<input type="text" class="form-control" name="room_no[]" value="{{old('room_no')}}" autofocus> 	     		       
			 		 	</div>
			 		 	<div class="form-group col-md-6 text-center">
			 		 		<label for="staff_id" class="control-label">နေရာချထားပေးမည့်ဝန်ထမ်း</label>
			 				<select name="staff_id[]" class="form-control selectpicker" data-live-search="true" id="staff_id">
			 					<option value="">နေရာချထားပေးမည့်ဝန်ထမ်း</option>
			 					@foreach($staffs as $result)
			 					<option value="{{$result->id}}" {{old('result_id') == $result->id ? 'selected' : ''}}>
			 						{{$result->name}} ->( {{$result->position->name}})({{$result->department->name}}/{{$result->ministry->name}})
			 					</option>
			 					@endforeach
			 		        </select>
			 		 	</div>
			 		 	<div class="form-group col-md-3 text-center">
			 		 		<label for="staff_id" class="control-label">Remark</label>
			 				<textarea name="remark" cols="25"></textarea>
			 		 	</div>
				 	</div>
				 	<div id="clone_four">
			 		 	<div class="form-group col-md-3 text-center">
			 		 		<label for="room_no" class=" control-label">အခန်းအမှတ်</label>	   
			 				<input type="text" class="form-control" name="room_no[]" value="{{old('room_no')}}" autofocus> 	     		       
			 		 	</div>
			 		 	<div class="form-group col-md-6 text-center">
			 		 		<label for="staff_id" class="control-label">နေရာချထားပေးမည့်ဝန်ထမ်း</label>
			 				<select name="staff_id[]" class="form-control selectpicker" data-live-search="true" id="staff_id">
			 					<option value="">နေရာချထားပေးမည့်ဝန်ထမ်း</option>
			 					@foreach($staffs as $result)
			 					<option value="{{$result->id}}" {{old('result_id') == $result->id ? 'selected' : ''}}>
			 						{{$result->name}} ->( {{$result->position->name}})({{$result->department->name}}/{{$result->ministry->name}})
			 					</option>
			 					@endforeach
			 		        </select>
			 		 	</div>
			 		 	<div class="form-group col-md-3 text-center">
			 		 		<label for="staff_id" class="control-label">Remark</label>
			 				<textarea name="remark" cols="25"></textarea>
			 		 	</div>
				 	</div>
				 	<div id="clone_five">
			 		 	<div class="form-group col-md-3 text-center">
			 		 		<label for="room_no" class=" control-label">အခန်းအမှတ်</label>	   
			 				<input type="text" class="form-control" name="room_no[]" value="{{old('room_no')}}" autofocus> 	     		       
			 		 	</div>
			 		 	<div class="form-group col-md-6 text-center">
			 		 		<label for="staff_id" class="control-label">နေရာချထားပေးမည့်ဝန်ထမ်း</label>
			 				<select name="staff_id[]" class="form-control selectpicker" data-live-search="true" id="staff_id">
			 					<option value="">နေရာချထားပေးမည့်ဝန်ထမ်း</option>
			 					@foreach($staffs as $result)
			 					<option value="{{$result->id}}" {{old('result_id') == $result->id ? 'selected' : ''}}>
			 						{{$result->name}} ->( {{$result->position->name}})({{$result->department->name}}/{{$result->ministry->name}})
			 					</option>
			 					@endforeach
			 		        </select>
			 		 	</div>
			 		 	<div class="form-group col-md-3 text-center">
			 		 		<label for="staff_id" class="control-label">Remark</label>
			 				<textarea name="remark" cols="25"></textarea>
			 		 	</div>
				 	</div>
	 			 	<!--  -->
				</div>
				<!--  -->
			</div>
        </div>
	</div>
	<!--  -->
</div>
@endsection