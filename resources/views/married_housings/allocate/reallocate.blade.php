@extends('layouts.master')
@section('content')
<div class="main-content">
	<!--  -->
    <div class="main-content-inner">
        <div class="page-content">
        	<div class="row">
	         	<!--  -->
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="text-info">
						({{$allocate->housing_category->name}})အိမ်ရာ/ 
						<b class="text-danger">
							[ တိုက်အမှတ် {{$allocate->building->building_no}} ]
						</b>၏အချက်အလက်များကို ပြင်ဆင်ခြင်း
					</h4>
					</div>
					<div class="panel-body">
						<div class="col-xs-12">
			               <form class="form-horizontal" method="POST" action="{{url('/married/allocate/'.$allocate->id)}}" enctype="multipart/form-data">
			               	   {{ csrf_field() }}
			               	   {{ method_field('PUT') }}
								<!--  -->
								<input type="hidden" name="ministry_id" value="{{$allocate->ministry_id}}">
							 	<div class="row">
							 		<div id="first_form" class="col-md-12">
							 		 	<div class="form-group col-md-3 text-center">
							 		 		<label for="room_no" class=" control-label">အခန်းအမှတ်</label>	   
							 				<input type="text" class="form-control" name="room_no" value="{{ $allocate->room_no }}"  autofocus required readonly> 
							 				@if ($errors->has('room_no.*'))
							 				<span class="help-block">
							 					<strong>{{ $errors->first('room_no.*') }}
							 					</strong>
							 				</span>
							 				@endif	     		       
							 		 	</div>
					 		 		 	<div class="form-group col-md-6 text-center">
					 		 		 		<label for="staff_id" class="control-label">နေရာချပေးထားသည့်ဝန်ထမ်း</label>
					 		 				<select name="staff_id" class="form-control selectpicker" data-live-search="true" id="staff_id" required>
					 		 					<option value="">နေရာချပေးထားသည့်ဝန်ထမ်း</option>
					 		 					@foreach($staffs as $result)
					 		 					<option value="{{$result->id}}">
					 		 						{{$result->name}} ->( {{$result->position->name}})({{$result->department->name}}/{{$result->ministry->name}})
					 		 					</option>
					 		 					@endforeach
					 		 		        </select>
					 		 		        @if ($errors->has('staff_id.*'))
					 		 		        <span class="help-block">
					 		 		        	<strong>{{ $errors->first('staff_id.*') }}
					 		 		        	</strong>
					 		 		        </span>
					 		 		        @endif
					 		 		 	</div>
							 		 	<div class="form-group col-md-3 text-center">
							 		 		<label for="staff_id" class="control-label">Remark</label>
							 				<textarea name="remark" cols="25">{{$allocate->remark}}</textarea>
							 		 	</div>
							 		</div>
							 	</div>
							 	<!--  -->
			                    <div class="form-group">
					              <div class="col-md-6 col-md-offset-5">
					                  <button type="submit" class="btn btn-success">
					                    <i class="ace-icon fa fa-check bigger-110"></i>
					                      Save
					                  </button>
					                  <a class="btn btn-danger" href="url({{'married/allocated-housings'}})" onclick="goBack()">
					                     <i class="ace-icon fa fa-undo bigger-110"></i>Cancel
					                  </a>
					              </div>
						        </div>
						        <!--  -->               	
			               </form>
						</div>
					</div>
					<!-- here is clonable forms -->
				</div>
				<!--  -->
			</div>
        </div>
	</div>
	<!--  -->
</div>
@endsection