@extends('layouts.master')
@section('content')
	<div class="main-content">
		<div class="main-content-inner">
			<div class="page-header">
				@if(session ('success'))
				<div id="successMessage" class="alert alert-success text-center col-md-4 pull-right">
					<button type="button" class="close" data-dismiss="alert">×</button>
					{{  session('success') }}
				</div>
				@endif
			</div>
	        <div class="page-content">
				<div class="row">
					<div class="panel panel-default">
						<div class="panel-heading">
							<div class="row">
								<div class="title text-info text-center">
									အဆောက်အဦးများစာရင်း
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									<a class="btn btn-primary btn-md" href="{{url('/married/buildings/create')}}">
										<i class="fa fa-fw fa-plus"></i>  
										အဆောက်အဦး အသစ်တစ်ခုထည့်သွင်းရန်
									</a>
								</div>
								<!--  -->
								<div class="col-md-4 col-md-4 text-center">
									<select name="category" id="category" class="form-control">
									    <option value="" selected>အိမ်ရာအမျိုးအစားအလိုက် ကြည့်ရန်</option>
									    @foreach($housing_categories as $result)
									    <option value="{{$result->id}}">{{$result->name}}</option>
									    @endforeach
									</select>
								</div>
								<!--  -->
								<div class="col-md-4 col-md-4 text-center">
									<select name="quarter" id="quarter" class="form-control">
									    <option value="" selected>တည်နေရာအလိုက် ကြည့်ရန်</option>
									    @foreach($quarters as $quarter)
									    <option value="{{$quarter->id}}">{{$quarter->name}}</option>
									    @endforeach
									</select>
								</div>
								<!--  -->
							</div>
						</div>
						<div class="panel-body">
							<div class="col-xs-12 table-responsive ">
								<table class="table table-bordered" id="buildings">
								    <thead>
								        <tr>
								        	<th>စဉ်</th>
								            <th>အဆောက်အဦးအမှတ်</th>
								            <th>အိမ်ရာအမျိုးအစား</th>
								            <th>တည်ရှိရာ (ရပ်ကွက်)</th>
								            <th>စီမံခန့်ခွဲရန်</th>
								        </tr>
								    </thead>
								</table>
							</div>
						</div>
						<div class="panel-footer">
							<a class="btn btn-danger" href="#" onclick="goBack()">
							     <i class="ace-icon fa fa-undo bigger-110"></i>ရှေ့စာမျက်နှာသို့
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
@section('js')
	<script>
			$(document).ready(function() {
			    oTable = $('#buildings').DataTable({
			        "processing": true,
			        "serverSide": true,
			        "ajax" :{
		    			        url: '{{ route("datatable.buildings") }}',
		    			        data: function (d) {
		    			            d.category = $('select[name=category]').val();
		    			            d.quarter = $('select[name=quarter]').val();
		    			        }
		    			    },
			        "columns": [
			        	{data: 'DT_Row_Index', orderable: false, searchable: false},
			            { data: 'building_no', name: 'building_no' },
			            { data: 'housing_category.name', name: 'housing_category.name',orderable: false },
			            { data: 'quarter.name', name: 'quarter.name',orderable: false },
			            {data: 'action', name: 'action',searchable: false,orderable:false}
			        ]
			    });
			    $('#category').on('change', function(e) {
			        oTable.draw();
			        e.preventDefault();
			    });
			    $('#quarter').on('change', function(e) {
			        oTable.draw();
			        e.preventDefault();
			    });
			});
	</script>
@endsection