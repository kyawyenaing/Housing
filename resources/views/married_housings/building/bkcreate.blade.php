@extends('layouts.master')
@section('content')
<div class="main-content">
	<!--  -->
      <div class="main-content-inner">
      	<!--  -->
      	<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="{{url('/user')}}">Home</a>
				</li>

				<li>
					<b class="text-info">
						အဆောက်အဦးအသစ်တစ်ခု ထည့်သွင်းခြင်း
					</b>
				</li>
			</ul>
		</div>
		<!--  -->
		<div class="page-header">
			<h4 class="text-info">
				အဆောက်အဦးအသစ်အတွက် အချက်အလက်များကို ဖြည့်သွင်းပါ
			</h4>
		</div>
		<!--  -->
        <div class="page-content">
        	<div class="row">
         	<!--  -->
			<div class="col-xs-12">
               <form class="form-horizontal" method="POST" action="{{url('/building')}}" enctype="multipart/form-data">
               	   {{ csrf_field() }}
					<!--  -->
				 	<div class="form-group {{ $errors->has('room_no') ? ' has-error' : '' }}">
				 		<label for="room_no" class="col-md-2 control-label">အခန်းအမှတ်</label>
						<div class="col-md-3">						   
							<input type="text" class="form-control" name="room_no[]" value="{{old('room_no')}}"> 	
							@if ($errors->has('room_no'))					   
							<span class="help-block">
								<strong>{{ $errors->first('room_no') }}</strong>
							</span>
							@endif    
						</div>
						<label for="staff_id" class="col-md-2 control-label">နေရာချထားပေးမည့်ဝန်ထမ်း</label>
						<div class="col-md-4">	
							<select name="staff_id" class="form-control selectpicker" data-live-search="true" required>
								<option value="">နေရာချထားပေးမည့်ဝန်ထမ်း</option>
								@foreach($staffs as $result)
								<option value="{{$result->id}}" {{old('result_id') == $result->id ? 'selected' : ''}}>
									{{$result->name}} -> {{$result->position->name}} -> {{$result->department->name}} -> {{$result->ministry->name}}
								</option>
								@endforeach
					        </select>					   
							@if ($errors->has('staff_id'))					   
							<span class="help-block">
								<strong>{{ $errors->first('staff_id') }}</strong>
							</span>
							@endif    
						</div>
					</div>
					<!--  -->
					<div class="form-group {{ $errors->has('room_no') ? ' has-error' : '' }}">
				 		<label for="room_no" class="col-md-2 control-label">အခန်းအမှတ်</label>
						<div class="col-md-3">						   
							<input type="text" class="form-control" name="room_no[]" value="{{old('room_no')}}"> 	
							@if ($errors->has('room_no'))					   
							<span class="help-block">
								<strong>{{ $errors->first('room_no') }}</strong>
							</span>
							@endif    
						</div>
						<label for="staff_id" class="col-md-2 control-label">နေရာချထားပေးမည့်ဝန်ထမ်း</label>
						<div class="col-md-4">	
							<select name="staff_id" class="form-control selectpicker" data-live-search="true" required>
								<option value="">နေရာချထားပေးမည့်ဝန်ထမ်း</option>
								@foreach($staffs as $result)
								<option value="{{$result->id}}" {{old('result_id') == $result->id ? 'selected' : ''}}>
									{{$result->name}} -> {{$result->position->name}} -> {{$result->department->name}} -> {{$result->ministry->name}}
								</option>
								@endforeach
					        </select>					   
							@if ($errors->has('staff_id'))					   
							<span class="help-block">
								<strong>{{ $errors->first('staff_id') }}</strong>
							</span>
							@endif    
						</div>
					</div>
					<!--  -->
                    <div class="form-group">
		              <div class="col-md-6 col-md-offset-4">
		                  <button type="submit" class="btn btn-success">
		                    <i class="ace-icon fa fa-check bigger-110"></i>
		                      Save
		                  </button>
		                  <a class="btn btn-danger" href="{{url('/building')}}">
		                     <i class="ace-icon fa fa-undo bigger-110"></i>Cancel
		                  </a>
		              </div>
			        </div>
			        <!--  -->               	
               </form>
			</div>
			<!--  -->
		</div>
        </div>
		<!--  -->
	</div>
	<!--  -->
</div>
<!--  -->
@endsection

<!--  -->
<!--  -->
			<div class="page-header">
				<div class="col-md-4 col-sm-4">
					<a class="btn btn-primary btn-sm" href="{{url('married/buildings/create')}}">
						<i class="fa fa-fw fa-plus"></i>  
						အဆောက်အဦးအသစ်တစ်ခု ထည့်သွင်းရန်
					</a>
				</div>
				<!--  -->
				<div class="col-md-4 col-sm-4 pull-left text-center">
					<select name="category" id="category" class="form-control">
					    <option value="" selected>တည်နေရာအလိုက် ကြည့်ရန်</option>
					    @foreach($housing_categories as $result)
					    <option value="{{$result->id}}">{{$result->name}}</option>
					    @endforeach
					</select>
				</div>
				<!--  -->
				<div class="col-md-4 col-sm-4 pull-left text-center">
					<select name="quarter" id="quarter" class="form-control">
					    <option value="" selected>တည်နေရာအလိုက် ကြည့်ရန်</option>
					    @foreach($quarters as $quarter)
					    <option value="{{$quarter->id}}">{{$quarter->name}}</option>
					    @endforeach
					</select>
				</div>
				<!--  -->
			</div>
			<!--  -->