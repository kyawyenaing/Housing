@extends('layouts.master')
@section('content')
<div class="main-content">
	<!--  -->
	<div class="main-content-inner">
	    <div class="page-content">
	    	<div class="row">
	    		<!--  -->
			    <div class="panel panel-default">
			    	<div class="panel-heading">
			    		<h4 class="text-info">
			    			{{$building->building_no}} ၏အချက်အလက်များကို ပြင်ဆင်ခြင်း
			    		</h4>
			    	</div>
			    	<div class="panel-body">
    				    <div class="col-xs-12">
    						<form class="form-horizontal" method="POST" action="{{url('married/buildings/'. $building->id)}}" enctype="multipart/form-data">
    							{{ csrf_field() }}
    							{{ method_field('PUT') }}
    							<div class="form-group {{ $errors->has('housing_category_id') ? ' has-error' : '' }}">
    								<label for="housing_category_id" class="col-md-4 control-label"> အိမ်ရာအမျိုးအစား</label>
    								<div class="col-md-6">	
    									<select name="housing_category_id" class="form-control selectpicker" data-live-search="true" required>
    										<option value=""> အိမ်ရာအမျိုးအစား</option>
    										@foreach($housing_categories as $result)
    										<option value="{{$result->id}}" {{$building->housing_category_id == $result->id ? 'selected' : ''}}>{{$result->name}}</option>
    										@endforeach
    							        </select>					   
    									@if ($errors->has('housing_category_id'))					   
    									<span class="help-block">
    										<strong>{{ $errors->first('housing_category_id') }}</strong>
    									</span>
    									@endif    
    								</div>
    							</div>
    							<!--  -->
    		               		<div class="form-group {{ $errors->has('quarter_id') ? ' has-error' : '' }}">
    								<label for="quarter_id" class="col-md-4 control-label">တည်နေရာ</label>
    								<div class="col-md-6">	
    									<select name="quarter_id" class="form-control selectpicker" data-live-search="true" required>
    										<option value="">တည်ရှိရာနေရာ</option>
    										@foreach($quarters as $result)
    										<option value="{{$result->id}}" {{$building->quarter_id == $result->id ? 'selected' : ''}}>{{$result->name}}</option>
    										@endforeach
    							        </select>					   
    									@if ($errors->has('quarter_id'))					   
    									<span class="help-block">
    										<strong>{{ $errors->first('quarter_id') }}</strong>
    									</span>
    									@endif    
    								</div>
    							</div>
    							<!--  -->
    							<div class="form-group {{ $errors->has('building_no') ? ' has-error' : '' }}">
    								<label for="building_no" class="col-md-4 control-label">တိုက်အမှတ်</label>
    								<div class="col-md-6">						   
    									<input  type="text" class="form-control" name="building_no" value="{{$building->building_no}}" required> 	
    									@if ($errors->has('building_no'))					   
    									<span class="help-block">
    										<strong>{{ $errors->first('building_no') }}</strong>
    									</span>
    									@endif    
    								</div>
    							</div>
    							<!--  -->
    							<div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
    								<label for="description" class="col-md-4 control-label">အသေးစိတ်ဖော်ပြချက်နှင့်မှတ်ချက်များ</label>
    								<div class="col-md-6">						   
    									<textarea class="form-control" rows="5" name="description" id="description">{{$building->description}}</textarea>
    									  @if ($errors->has('description'))					   
    									<span class="help-block">
    										<strong>{{ $errors->first('description') }}</strong>
    									</span>
    									@endif
    								</div>
    							</div>
    							<!--  -->
    							<div class="form-group">
    								<div class="col-md-6 col-md-offset-4">
    									<button type="submit" class="btn btn-success">
    										<i class="ace-icon fa fa-check bigger-110"></i>
    										Update
    									</button>
    									<a class="btn btn-danger" href="{{url('/building')}}">
    										<i class="ace-icon fa fa-undo bigger-110"></i>Cancel
    									</a>
    								</div>
    							</div>
    						</form>
    					</div>
			    	</div>
			    </div>
			</div>
			<!--  -->
	    </div>
		<!--  -->
	</div>
	<!--  -->
</div>
<!--  -->
@endsection