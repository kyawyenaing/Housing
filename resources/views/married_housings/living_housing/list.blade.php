@extends('layouts.master')
@section('content')
<div class="main-content">
	<!--  -->
	<div class="main-content-inner">
		<!--  -->
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="{{url('/home')}}">Home</a>
				</li>
				<li>
					<b class="text-info">
						({{$housing_category->name}})အမျိုးအစား အိမ်ခန်းများစာရင်း
					</b>
					<input type="hidden" name="" value="{{$housing_category->url}}" id="category">
				</li>							
			</ul>
		</div>
		<!--  -->
		<div class="row">
			@if(session ('success'))
			<div id="successMessage" class="alert alert-success text-center">
				<button type="button" class="close" data-dismiss="alert">×</button>
				{{  session('success') }}
			</div>
			@endif
		</div>
		<!--  -->
		<div class="page-header">
			<!--  -->
			<div class="col-md-4 col-sm-4 pull-left text-center">
				<select name="quarter" id="quarter" class="form-control">
				    <option value="" selected>တည်နေရာအလိုက် ကြည့်ရန်</option>
				    @foreach($quarters as $quarter)
				    <option value="{{$quarter->id}}">{{$quarter->name}}</option>
				    @endforeach
				</select>
			</div>
			<!--  -->
			<div class="col-md-4 col-sm-4 pull-right">
				<button class="btn btn-primary btn-sm btn-bold" onclick="printDiv('print_area')" >
					<i class="ace-icon fa fa-print bigger-130 blue"></i>Print
				</button>
			</div>
			<!--  -->
		</div>
		<!--  -->
        <div class="page-content">
        	<!--  -->
        	<div class="row">
        		<div class="col-xs-12">
        			<b class="text-center pull-left" id="total"></b>
        		</div>
        	</div>
			<div class="row">
				<!--  -->
				<div class="col-xs-12 table-responsive">
					<table class="table table-bordered text-center" id="initial">
					    <thead>
					        <tr>
								<th>တိုက်အမှတ်</th>	
								<th>အခန်းအမှတ်</th>
								<th>နေထိုင်သူအမည်</th>
								<th>ရာထူး</th>
								<th>ဦးစီးဌာန</th>
								<th>ဝန်ကြီးဌာန</th>
								<th>စတင်နေထိုင်သည့်ရက်စွဲ</th>
								<th>မှတ်ချက်</th>					
					        </tr>
					    </thead>
					    <tbody>
					    	@foreach($living_housings as $result)
					    		<tr>
					    			<td>{{$result->building->name}}</td>
					    			<td>{{$result->room_no}}</td>
					    			<td>{{$result->staff->name}}</td>
					    			<td>{{$result->staff->position->name}}</td>
					    			<td>{{$result->staff->department->name}}</td>
					    			<td>{{$result->staff->ministry->name}}</td>
					    			<td>{{date('d-m-Y',strtotime($result->living_date))}}</td>
					    			<td>{{$result->description}}</td>
					    		</tr>
					    	@endforeach
					    </tbody>
					</table>
				</div>
				<!--  -->
			</div>
			<!--  -->
		</div>
		<!--  -->
	</div>
	<!--  -->
</div>
<!--  -->
@endsection('content')
@section('js')
	$(document).ready(function(e) {
	    $('#quarter').on('change', function(e) {
	    var link = window.location.origin+"/NPTDC-Housing/married/live_"+$('#category').val()+'/'+$(this).val();
	    window.location.href = link;
	     e.preventDefault();
	    });
	});
@endsection