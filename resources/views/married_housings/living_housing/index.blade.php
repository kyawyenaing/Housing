@extends('layouts.master')
@section('content')
<div class="main-content">
	<!--  -->
	<div class="main-content-inner">
		<!--  -->
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="{{url('/home')}}">Home</a>
				</li>
				<li>
					<b class="text-info">

					</b>
				</li>							
			</ul>
		</div>
		<!--  -->
		<div class="page-header">
			<!--  -->
			<div class="col-md-4 col-sm-4 pull-right">
				<button class="btn btn-primary btn-sm btn-bold" onclick="printDiv('print_area')" >
					<i class="ace-icon fa fa-print bigger-130 blue"></i>Print
				</button>
			</div>
			<!--  -->
		</div>
		<!--  -->
        <div class="page-content">
        	<!--  -->
        	<div class="row">
        		<div class="col-xs-12">
        			<b class="text-center pull-left" id="total"></b>
        		</div>
        	</div>
			<div class="row">
				<!--  -->
				<div class="col-xs-12 table-responsive">
					<table class="table table-bordered text-center" id="initial">
					    <thead>
					        <tr>
								<th class="center">စဉ်</th>					
								<th class="center">ဝန်ကြီးဌာန</th>
								<th class="center">ဝန်ကြီး</th>
								<th>ဒု-ဝန်ကြီး</th>
								<th class="center">Duplex</th>
								<th colspan="7" class="center">2U3S</th>
								<th colspan="4" class="center">4U4S(3B)</th>
								<th colspan="7" class="center">6U4S(3B)</th>
								<th colspan="3" class="center">4U4S(2B)</th>
								<th colspan="6" class="center">6U4S(2B)</th>
					        </tr>
					    </thead>
					    <tbody>
					    	<tr>
					    		<th></th>
								<th></th>
								<!-- one -->
								<th></th>
								<!-- two -->
								<th></th>
								<!-- three -->
								<th></th>
								<!-- four -->
								<th>စံပယ်</th>
								<th>P1</th>
								<th>P2</th>
								<th>P3</th>
								<th>P4</th>
								<th>P5</th>
								<th>ပေါင်း</th>
								<!-- five -->
								<th>ကံ့ကော်</th>
								<th>ပိတောက်</th>
								<th>ခရေ</th>
								<th>ပေါင်း</th>
								<!-- six -->
								<th>ကံ့ကော်</th>
								<th>P1</th>
								<th>P2</th>
								<th>P3</th>
								<th>P4</th>
								<th>P5</th>
								<th>ပေါင်း</th>
								<!-- seven -->
								<th>ကံ့ကော်</th>
								<th>ပိတောက်</th>
								<th>ပေါင်း</th>
								<!-- eight -->
								<th>P1</th>
								<th>P2</th>
								<th>P3</th>
								<th>P4</th>
								<th>P5</th>
								<th>ပေါင်း</th>
								<!-- nine -->
					    	</tr>
					    	<!-- row one -->
					    	<tr>
					    		<td>{{1}}</td>
					    		<td>{{$data['one']['ministry_one']}}</td>
					    		<td>{{$data['one']['housing_one']}}</td>
					    		<td>{{$data['one']['housing_one']}}</td>
					    		<td>{{$data['one']['housing_two']}}</td>
					    		<td>{{$data['one']['housing_three']}}</td>
					    		<!--  -->
					    		<td>{{$data['one']['housing_four_one']}}</td>
					    		<td>{{$data['one']['housing_four_two']}}</td>
					    		<td>{{$data['one']['housing_four_three']}}</td>
					    		<td>{{$data['one']['housing_four_four']}}</td>
					    		<td>{{$data['one']['housing_four_five']}}</td>
					    		<td>{{$data['one']['housing_four_six']}}</td>
					    		<td>{{$data['one']['total_housing_four']}}</td>
					    		<!--  -->
					    		<td>{{$data['one']['housing_five_one']}}</td>
					    		<td>{{$data['one']['housing_five_two']}}</td>
					    		<td>{{$data['one']['housing_five_three']}}</td>
					    		<!--  -->
					    		<td>{{$data['one']['housing_six_one']}}</td>
					    		<td>{{$data['one']['housing_six_two']}}</td>
					    		<td>{{$data['one']['housing_six_three']}}</td>
					    		<td>{{$data['one']['housing_six_four']}}</td>
					    		<td>{{$data['one']['housing_six_five']}}</td>
					    		<td>{{$data['one']['housing_six_six']}}</td>
					    		<td>{{$data['one']['total_housing_six']}}</td>
					    		<!--  -->
					    		<td>{{$data['one']['housing_seven_one']}}</td>
					    		<td>{{$data['one']['housing_seven_two']}}</td>
					    		<td>{{$data['one']['total_housing_seven']}}</td>
					    		<!--  -->
					    		<td>{{$data['one']['housing_eight_one']}}</td>
					    		<td>{{$data['one']['housing_eight_two']}}</td>
					    		<td>{{$data['one']['housing_eight_three']}}</td>
					    		<td>{{$data['one']['housing_eight_four']}}</td>
					    		<td>{{$data['one']['housing_eight_five']}}</td>
					    		<td>{{$data['one']['total_housing_eight']}}</td>
					    		<!--  -->
					    	</tr>
					    	<!-- row two -->
					    	<tr>
					    		<td>{{2}}</td>
					    		<td>{{$data['two']['ministry_two']}}</td>
					    		<td>{{$data['two']['housing_one']}}</td>
					    		<td>{{$data['two']['housing_one']}}</td>
					    		<td>{{$data['two']['housing_two']}}</td>
					    		<td>{{$data['two']['housing_three']}}</td>
					    		<!--  -->
					    		<td>{{$data['two']['housing_four_one']}}</td>
					    		<td>{{$data['two']['housing_four_two']}}</td>
					    		<td>{{$data['two']['housing_four_three']}}</td>
					    		<td>{{$data['two']['housing_four_four']}}</td>
					    		<td>{{$data['two']['housing_four_five']}}</td>
					    		<td>{{$data['two']['housing_four_six']}}</td>
					    		<td>{{$data['two']['total_housing_four']}}</td>
					    		<!--  -->
					    		<td>{{$data['two']['housing_five_one']}}</td>
					    		<td>{{$data['two']['housing_five_two']}}</td>
					    		<td>{{$data['two']['housing_five_three']}}</td>
					    		<!--  -->
					    		<td>{{$data['two']['housing_six_one']}}</td>
					    		<td>{{$data['two']['housing_six_two']}}</td>
					    		<td>{{$data['two']['housing_six_three']}}</td>
					    		<td>{{$data['two']['housing_six_four']}}</td>
					    		<td>{{$data['two']['housing_six_five']}}</td>
					    		<td>{{$data['two']['housing_six_six']}}</td>
					    		<td>{{$data['two']['total_housing_six']}}</td>
					    		<!--  -->
					    		<td>{{$data['two']['housing_seven_one']}}</td>
					    		<td>{{$data['two']['housing_seven_two']}}</td>
					    		<td>{{$data['two']['total_housing_seven']}}</td>
					    		<!--  -->
					    		<td>{{$data['two']['housing_eight_one']}}</td>
					    		<td>{{$data['two']['housing_eight_two']}}</td>
					    		<td>{{$data['two']['housing_eight_three']}}</td>
					    		<td>{{$data['two']['housing_eight_four']}}</td>
					    		<td>{{$data['two']['housing_eight_five']}}</td>
					    		<td>{{$data['two']['total_housing_eight']}}</td>
					    		<!--  -->
					    	</tr>
					    </tbody>
					</table>
				</div>
				<!--  -->
			</div>
			<!--  -->
		</div>
		<!--  -->
	</div>
	<!--  -->
</div>
<!--  -->
@endsection('content')
@section('js')
	$(document).ready(function(e) {
	    $('#category').on('change', function(e) {
	    if($(this).val() != 'null') {
	    	console.log("I'm one");
	    var link = window.location.origin+"/NPTDC-Housing/married/live_"+$(this).val();
	    window.location.href = link ;
	    }
	    e.preventDefault();		    
	    });
	});
@endsection