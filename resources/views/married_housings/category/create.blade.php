@extends('layouts.master')
@section('content')
<div class="main-content">
	<!--  -->
      <div class="main-content-inner">
      	<!--  -->
        <div class="page-content">
	        <div class="row">
	         	<!--  -->
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="text-info">အိမ်ရာအမျိုးအစားအသစ်တစ်ခု ထည့်သွင်းခြင်း</h4>
					</div>
					<div class="panel-body">
						<div class="col-xs-12">
			               <form class="form-horizontal" method="POST" action="{{url('/housing_category')}}" enctype="multipart/form-data">
			               	   {{ csrf_field() }}
			               		<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
									<label for="name" class="col-md-4 control-label">အိမ်ရာအမျိုးအစား(Myanmar)</label>
									<div class="col-md-6">						   
										<input type="text" class="form-control" name="name" value="{{old('name')}}" required> 	
										@if ($errors->has('name'))					   
										<span class="help-block">
											<strong>{{ $errors->first('name') }}</strong>
										</span>
										@endif    
									</div>
								</div>
								<!--  -->
								<div class="form-group {{ $errors->has('short_name') ? ' has-error' : '' }}">
									<label for="name" class="col-md-4 control-label">
										အတိုကောက်အမည်
									</label>

									<div class="col-md-6">						   
										<input id="short_name" type="text" class="form-control" name="short_name" value="{{old('short_name')}}" required>  	
										@if ($errors->has('short_name'))					   
										<span class="help-block">
											<strong>{{ $errors->first('short_name') }}</strong>
										</span>
										@endif    
									</div>
								</div>
								<!--  -->
								 <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
									<label for="name" class="col-md-4 control-label">
										အသေးစိတ်ဖော်ပြချက်(သို့)မှတ်ချက်များ
									</label>
									<div class="col-md-6">						   
										<textarea class="form-control" rows="5" name="description" id="description">{{old('description')}}</textarea>
										  @if ($errors->has('description'))					   
										<span class="help-block">
											<strong>{{ $errors->first('description') }}</strong>
										</span>
										@endif
									</div>
								</div>
								<!--  -->
			                    <div class="form-group">
					              <div class="col-md-6 col-md-offset-4">
					                  <button type="submit" class="btn btn-success">
					                    <i class="ace-icon fa fa-check bigger-110"></i>
					                      Save
					                  </button>
					                  <a class="btn btn-danger" href="{{url('/housing_category')}}">
					                     <i class="ace-icon fa fa-undo bigger-110"></i>Cancel
					                  </a>
					              </div>
						        </div>
						        <!--  -->               	
			               </form>
						</div>
					</div>
				</div>
				<!--  -->
			</div>
        </div>
		<!--  -->
	</div>
	<!--  -->
</div>
<!--  -->
@endsection