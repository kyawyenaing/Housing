@extends('layouts.master')
@section('content')
	<div class="main-content">
		<div class="main-content-inner">
	        <div class="page-content">
				<div class="row">
					<div class="panel panel-default">
						<div class="panel-heading">
							<div class="title text-info text-center">
								တည်နေရာ ရပ်ကွက်များစာရင်း
							</div>
							<a class="btn btn-primary" href="{{url('/quarter/create')}}">
								<i class="fa fa-fw fa-plus"></i>  
								တည်နေရာ ရပ်ကွက် အသစ်တစ်ခုထည့်သွင်းရန်
							</a>
							@if(session ('success'))
							<div id="successMessage" class="alert alert-success text-center col-md-4 pull-right">
								<button type="button" class="close" data-dismiss="alert">×</button>
								{{  session('success') }}
							</div>
							@endif
						</div>
						<div class="panel-body">
							<div class="col-xs-12 table-responsive ">
								<table class="table table-bordered" id="quarters">
								    <thead>
								        <tr>
								            <th>စဉ်</th>
								            <th>ရပ်ကွက်အမည်</th>
								            <th>ရပ်ကွက်အမည်(အတိုကောက်)</th>
								            <th>အသေးစိတ်ဖော်ပြချက်(သို့)မှတ်ချက်များ</th>
								            <th>စီမံခန့်ခွဲရန်</th>
								        </tr>
								    </thead>
								</table>
							</div>
						</div>
						<div class="panel-footer">
							<a class="btn btn-danger" href="#" onclick="goBack()">
							     <i class="ace-icon fa fa-undo bigger-110"></i>ရှေ့စာမျက်နှာသို့
							</a>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
@endsection
@section('js')
	<script>
		$(document).ready(function() {
		    oTable = $('#quarters').DataTable({
		        "processing": true,
		        "serverSide": true,
		        "ajax": "{{ route('datatable.quarter') }}",
		        "columns": [
		            {data: 'DT_Row_Index', orderable: false, searchable: false},
		            { data: 'name', name: 'name' },
		            { data: 'short_name', name: 'short_name' },
		            { data: 'description', name: 'description'},
		            {data: 'action', name: 'action',searchable: false,orderable:false}
		        ]
		    });
		});
	</script>
@endsection