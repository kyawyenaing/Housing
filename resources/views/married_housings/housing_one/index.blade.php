@extends('layouts.master')
@section('content')
<div class="main-content">
	<!--  -->
	<div class="main-content-inner">
		<!--  -->
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="{{url('/home')}}">Home</a>
				</li>
				<li>
					<b class="text-info">
						({{$housing_category->name}})အိမ်ရာများစာရင်း
					</b>
				</li>							
			</ul>
		</div>
		<!--  -->
		<div class="row">
			@if(session ('success'))
			<div id="successMessage" class="alert alert-success text-center">
				<button type="button" class="close" data-dismiss="alert">×</button>
				{{  session('success') }}
			</div>
			@endif
		</div>
		<!--  -->
		<div class="page-header">
			<!--  -->
			<div class="col-md-4 col-sm-4">
				<a class="btn btn-primary btn-sm" href="{{url('married/housing_one/create')}}">
					<i class="fa fa-fw fa-plus"></i>  
					အိမ်ခန်းအသစ်တစ်ခု ထည့်သွင်းရန်
				</a>
			</div>
			<!--  -->
			<div class="col-md-4 col-sm-4 pull-left text-center">
				<select name="quarter" id="quarter" class="form-control">
				    <option value="" selected>တည်နေရာအလိုက် ကြည့်ရန်</option>
				    @foreach($quarters as $quarter)
				    <option value="{{$quarter->id}}">{{$quarter->name}}</option>
				    @endforeach
				</select>
			</div>
			<!--  -->
			<div class="col-md-4 col-sm-4 pull-left">
				<select name="allocate" id="allocate" class="form-control">
				    <option value="" selected>နေရာချထားမှုအလိုက် ကြည့်ရန်</option>
				    <option value="1">အိမ်ခန်းအလွတ်များ</option>
				    <option value="2">နေရာချထားပြီးသော အိမ်ခန်းများ</option>
				</select>
			</div>
			<!--  -->
		</div>
		<!--  -->
        <div class="page-content">
        	<!--  -->
			<div class="row">
				<!--  -->
				<div class="col-xs-12 table-responsive">
					<table class="table table-bordered text-center" id="housing_one">
					    <thead>
					        <tr>
					            <th>တည်နေရာ</th>
								<th>တိုက်အမှတ်</th>	
								<th>အခန်းအမှတ်</th>
								<th>အသေးစိတ်ဖော်ပြချက်(သို့)မှတ်ချက်များ</th>			
								<th>စီမံခန့်ခွဲရန်</th>
					        </tr>
					    </thead>
					</table>
				</div>
				<!--  -->
			</div>
			<!--  -->
		</div>
		<!--  -->
	</div>
	<!--  -->
</div>
<!--  -->
@endsection('content')
@section('js')
	$(document).ready(function() {
	    oTable = $('#housing_one').DataTable({
	        "processing": true,
	        "serverSide": true,
            "ajax" :{
    			        url: '{{ route("datatable.housing_one") }}',
    			        data: function (d) {
    			            d.quarter = $('select[name=quarter]').val();
    			            d.allocate = $('select[name=allocate]').val();
    			        }
    			    },
	        "columns": [
	        	{ data: 'quarter_name', name: 'quarter_name',searchable:false },
	            { data: 'building_no', name: 'building_no',searchable:false },
	            { data: 'room_no', name: 'room_no' },
	            { data: 'description', name: 'description',orderable:false },
	            { data: 'action', name: 'action',searchable: false, orderable: false}
	        ]
	    });
	    $('#quarter').on('change', function(e) {
	        oTable.draw();
	        e.preventDefault();
	    });
	    $('#allocate').on('change', function(e) {
	        oTable.draw();
	        e.preventDefault();
	    });

	});
@endsection