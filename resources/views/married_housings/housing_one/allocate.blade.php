@extends('layouts.master')
@section('content')
<div class="main-content">
	<!--  -->
	<div class="main-content-inner">
		<!--  -->
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="{{url('/home')}}">Home</a>
				</li>
				<li>
					<b class="text-info">{{$housing_one->housing_category->name}}အိမ်ရာ ({{$housing_one->quarter->name}}) |  တိုက်အမှတ်({{$housing_one->building->name}}) | အခန်းအမှတ် ({{$housing_one->room_no}}) တွင်ဝန်ထမ်းကို နေရာချပေးထားခြင်း</b>
				</li>							
			</ul>
		</div>
		<!--  -->
		<div class="page-header">
			<h4 class="text-info">ဝန်ထမ်းကို နေရာချပေးထားခြင်းအတွက် အချက်အလက်များကို ဖြည့်သွင်းပါ</h4>
		</div>
        <div class="page-content">
        	<!--  -->
        	<div class="row">
        		<!--  -->
				<div class="col-xs-12">
	               <form class="form-horizontal" method="POST" action="{{url('married/housing_one/confirm_allocate')}}" enctype="multipart/form-data">
	               	   {{ csrf_field() }}
	               	   <input type="hidden" name="id" value="{{$housing_one->id}}">
	               	   <!--  -->
	               		<div class="form-group {{ $errors->has('building_id') ? ' has-error' : '' }}">
							<label for="building_id" class="col-md-4 control-label">တိုက်အမှတ်</label>
							<div class="col-md-6">						   
								<input  type="text" class="form-control" name="building_id" value="{{$housing_one->building->name}}" readonly> 	
								@if ($errors->has('building_id'))					   
								<span class="help-block">
									<strong>{{ $errors->first('building_id') }}</strong>
								</span>
								@endif    
							</div>
						</div>
						<!--  -->
						<div class="form-group {{ $errors->has('room_no') ? ' has-error' : '' }}">
							<label for="room_no" class="col-md-4 control-label">အခန်းအမှတ်</label>
							<div class="col-md-6">						   
								<input  type="text" class="form-control" name="room_no" value="{{$housing_one->room_no}}" readonly>
								@if ($errors->has('room_no'))					   
								<span class="help-block">
									<strong>{{ $errors->first('room_no') }}</strong>
								</span>
								@endif    
							</div>
						</div>
						<!--  -->
						<div class="form-group {{ $errors->has('staff_id') ? ' has-error' : '' }}">
							<label for="staff_id" class="col-md-4 control-label">ဝန်ထမ်းအမည်</label>
							<div class="col-md-6">	
								<select name="staff_id" class="form-control selectpicker" data-live-search="true" required>
									<option value="">နေရာချပေးထားမည့်ဝန်ထမ်း</option>
									@foreach($staffs as $result)
									<option value="{{$result->id}}" {{old('staff_id') == $result->id ? 'selected' : ''}}>
										{{$result->name}} -> {{$result->position->name}} -> {{$result->department->name}} -> {{$result->ministry->name}}
									</option>
									@endforeach
						        </select>					   
								@if ($errors->has('staff_id'))					   
								<span class="help-block">
									<strong>{{ $errors->first('staff_id') }}</strong>
								</span>
								@endif    
							</div>
						</div>
						<!--  -->
						<div class="form-group{{ $errors->has('living_date') ? ' has-error' : '' }}">
							<label class="col-md-4 control-label">
								စတင်နေထိုင်မည့် ရက်စွဲ
							</label>
							<div class="col-md-6">	      						
								<div class="input-group">
									<input type="text" class="form-control" id="living_date"  value="{{old('living_date')}}" name="living_date" required />
									<span class="input-group-addon" >
										<i class="fa fa-calendar" aria-hidden="true"></i>
									</span>            				
								</div>
								@if ($errors->has('living_date'))
								<span class="help-block">
									<strong>{{ $errors->first('living_date') }}
									</strong>
								</span>
								@endif 
							</div>
						</div>
						<!--  -->
						<div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
							<label for="description" class="col-md-4 control-label">မှတ်ချက်</label>
							<div class="col-md-6">						   
								<textarea class="form-control" rows="5" name="description" id="description">{{$housing_one->description}}</textarea>
								  @if ($errors->has('description'))					   
								<span class="help-block">
									<strong>{{ $errors->first('description') }}</strong>
								</span>
								@endif
							</div>
						</div>
						<!--  -->
	                    <div class="form-group">
				              <div class="col-md-6 col-md-offset-4">
				                  <button type="submit" class="btn btn-success">
				                    <i class="ace-icon fa fa-check bigger-110"></i>
				                      Save
				                  </button>
				                  <a class="btn btn-danger" href="{{url('married/housing_one')}}">
				                     <i class="ace-icon fa fa-undo bigger-110"></i>Cancel
				                  </a>
				              </div>
				        </div> 
				        <!--  -->
	               </form>
				</div>
				<!--  -->
        	</div>
        	<!--  -->
		</div>
		<!--  -->
	</div>
	<!--  -->
</div>
<!--  -->
@endsection
