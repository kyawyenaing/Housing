@extends('layouts.master')
@section('content')
<div class="main-content">
	<!--  -->
	<div class="main-content-inner">
		<!--  -->
		<div class="page-content">
		    <div class="row">
		    	<!--  -->
			    <div class="panel panel-default">
			    	<div class="panel-heading">
			    		<h4 class="text-info">
			    			{{$position->name}} ရာထူး ၏အချက်အလက်များကို ပြင်ဆင်ခြင်း
			    		</h4>
			    	</div>
			    	<div class="panel-body">
		    		    <div class="col-xs-12">
		    				<form class="form-horizontal" method="POST" action="{{url('/position/'. $position->id)}}" enctype="multipart/form-data">
		    					{{ csrf_field() }}
		    					{{ method_field('PUT') }}
		    					<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
		    						<label for="name" class="col-md-4 control-label">ရာထူးအမည်</label>

		    						<div class="col-md-6">						   
		    							<input id="name" type="text" class="form-control" name="name" value="{{$position->name}}"> 	
		    							@if ($errors->has('name'))					   
		    							<span class="help-block">
		    								<strong>{{ $errors->first('name') }}</strong>
		    							</span>
		    							@endif    
		    						</div>
		    					</div>
		    					<!--  -->
		    					<div class="form-group {{ $errors->has('name_en') ? ' has-error' : '' }}">
		    						<label for="name_en" class="col-md-4 control-label">ရာထူးအမည်(English)</label>
		    						<div class="col-md-6">						   
		    							<input id="name_en" type="text" class="form-control" name="name_en" value="{{$position->name_en}}"> 	
		    							@if ($errors->has('name_en'))					   
		    							<span class="help-block">
		    								<strong>{{ $errors->first('name_en') }}</strong>
		    							</span>
		    							@endif    
		    						</div>
		    					</div>
		    					<!--  -->
		    					<div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
		    						<label for="description" class="col-md-4 control-label"> အသေးစိတ်ဖော်ပြချက်(သို့)မှတ်ချက်များ </label>
		    						<div class="col-md-6">						   
		    							<textarea class="form-control" rows="5" name="description" id="description">{{$position->description}}</textarea>
		    							  @if ($errors->has('description'))					   
		    							<span class="help-block">
		    								<strong>{{ $errors->first('description') }}</strong>
		    							</span>
		    							@endif
		    						</div>
		    					</div>
		    					<!--  -->
		    										
		    					<div class="form-group">
		    						<div class="col-md-6 col-md-offset-4">
		    							<button type="submit" class="btn btn-success">
		    								<i class="ace-icon fa fa-check bigger-110"></i>
		    								Save
		    							</button>
		    							<a class="btn btn-danger" href="{{url('/position')}}">
		    								<i class="ace-icon fa fa-undo bigger-110"></i>Cancel
		    							</a>
		    						</div>
		    					</div>
		    				</form>
		    			</div>
			    	</div>
			    </div>
			</div>
		</div>
	</div>
</div>
@endsection