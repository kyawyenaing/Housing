@extends('layouts.master')
@section('content')
<div class="main-content">
	<!--  -->
      <div class="main-content-inner">
      	<!--  -->
        <div class="page-content">
        	<div class="row">
         	<!--  -->
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="text-info">
						ဝန်ထမ်းအသစ်တစ်ယောက် ထည့်သွင်းခြင်း
					</h4>
				</div>
				<div class="panel-body">
					<div class="col-xs-12">
		               <form class="form-horizontal" method="POST" action="{{url('/staff')}}" enctype="multipart/form-data">
		               	   {{ csrf_field() }}
		               		

							<!--  -->
		               		<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
								<label for="name" class="col-md-4 control-label">
									ဝန်ထမ်းအမည်
								</label>

								<div class="col-md-6">						   
									<input id="name" type="text" class="form-control" name="name" value="{{old('name')}}" required> 	
									@if ($errors->has('name'))					   
									<span class="help-block">
										<strong>{{ $errors->first('name') }}</strong>
									</span>
									@endif    
								</div>
							</div>
							<!--  -->
							 <div class="form-group {{ $errors->has('name_en') ? ' has-error' : '' }}">
								<label for="name_en" class="col-md-4 control-label">
									ဝန်ထမ်းအမည်(English)
								</label>
								<div class="col-md-6">						   
									<input id="name_en" type="text" class="form-control" name="name_en" value="{{old('name_en')}}"> 	
									@if ($errors->has('name_en'))					   
									<span class="help-block">
										<strong>{{ $errors->first('name_en') }}</strong>
									</span>
									@endif    
								</div>
							</div>
							<!--  -->
							<div class="form-group {{ $errors->has('position_id') ? ' has-error' : '' }}">
								<label for="position_id" class="col-md-4 control-label">ရာထူး</label>
								<div class="col-md-6">					
									<select id="position_id" name="position_id" class="form-control selectpicker" data-live-search="true" required>
										<option value="">ဝန်ထမ်း၏ ရာထူးကို ရွေးချယ်ပေးပါ</option>
										@foreach($positions as $position)
										<option value="{{$position->id}}" {{old('position_id') == $position->id ? 'selected' : ''}}>{{$position->name}}</option>
										@endforeach
									</select>			
									@if ($errors->has('position_id'))					   
									<span class="help-block">
										<strong>{{ $errors->first('position_id') }}</strong>
									</span>
									@endif    
								</div>
							</div>
							<!--  -->
							<div class="form-group {{ $errors->has('department_id') ? ' has-error' : '' }}">
								<label for="department_id" class="col-md-4 control-label">ဦးစီးဌာန</label>
								<div class="col-md-6">						   
								<select id="department_id" name="department_id" class="form-control selectpicker" data-live-search="true" required>
										<option value="">ဝန်ထမ်း၏ ဦးစီးဌာန ရွေးချယ်ပေးပါ</option>
										@foreach($departments as $department)
										<option value="{{$department->id}}" {{old('department_id') == $department->id ? 'selected' : ''}}>{{$department->name}}/{{$department->ministry->name}}ဝန်ကြီးဌာန</option>
										@endforeach
									</select>		
									@if ($errors->has('department_id'))					  
									<span class="help-block">
										<strong>{{ $errors->first('department_id') }}</strong>
									</span>
									@endif    
								</div>
							</div>
							<!--  -->
							<div class="form-group {{ $errors->has('address') ? ' has-error' : '' }}">
								<label for="address" class="col-md-4 control-label">လိပ်စာ</label>
								<div class="col-md-6">						   
									<textarea class="form-control" rows="5" name="address" id="address" required>{{old('address')}}</textarea>
									  @if ($errors->has('address'))					   
									<span class="help-block">
										<strong>{{ $errors->first('address') }}</strong>
									</span>
									@endif   
								</div>
							</div>
							<!--  -->
		                    <div class="form-group">
					            <div class="col-md-6 col-md-offset-4">
					                <button type="submit" class="btn btn-success">
					                    <i class="ace-icon fa fa-check bigger-110"></i>
					                      Save
					                </button>
					                <a class="btn btn-danger" href="{{url('/staff')}}">
					                     <i class="ace-icon fa fa-undo bigger-110"></i> Cancel
					                </a>
					            </div>
					        </div>
		          			<!--  -->
		               </form>
					</div>
				</div>
			</div>
		</div>
        </div>
		<!--  -->
	</div>
	<!--  -->
</div>
<!--  -->
@endsection