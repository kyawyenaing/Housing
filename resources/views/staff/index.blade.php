@extends('layouts.master')
@section('content')
	<div class="main-content">
		<!--  -->
		<div class="main-content-inner">
	        <div class="page-content">
				<div class="row">
					<div class="panel panel-default">
						<div class="panel-heading">
							<div class="title text-info text-center">
								ဝန်ထမ်းများစာရင်း
							</div>
							<a class="btn btn-primary" href="{{url('/staff/create')}}">
								<i class="fa fa-fw fa-plus"></i>  
								ဝန်ထမ်းအသစ်တစ်ယောက် ထည့်သွင်းရန်
							</a>
							@if(session ('success'))
							<div id="successMessage" class="alert alert-success text-center col-md-4 pull-right">
								<button type="button" class="close" data-dismiss="alert">×</button>
								{{  session('success') }}
							</div>
							@endif
						</div>
						<div class="panel-body">
							<div class="col-xs-12 table-responsive ">
								<table class="table table-bordered" id="users">
								    <thead>
								        <tr>
								            <th>စဉ်</th>
								            <th>ဝန်ထမ်းအမည်</th>
								            <th>ဝန်ထမ်းအမည်(English) </th>
								            <th>ရာထူး</th>
								            <th>ဦးစီးဌာန</th>
								            <th>ဝန်ကြီးဌာန</th>
								            <th>စီမံခန့်ခွဲရန်</th>
								        </tr>
								    </thead>
								</table>
							</div>
						</div>
						<div class="panel-footer">
							<a class="btn btn-danger" href="#" onclick="goBack()">
							     <i class="ace-icon fa fa-undo bigger-110"></i>ရှေ့စာမျက်နှာသို့
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
@section('js')
	<script>
		$(document).ready(function() {
		    oTable = $('#users').DataTable({
		        "processing": true,
		        "serverSide": true,
		        "ajax": "{{ route('datatable.staff') }}",
		        "columns": [
		            {data: 'DT_Row_Index', orderable: false, searchable: false},
			        { data: 'name', name: 'name'},
		            { data: 'name_en', name: 'name_en' },
		            { data: 'position.name', name: 'position.name',searchable: false ,orderable:false},
		            { data: 'department.name', name: 'department.name',searchable: false,orderable: false },
		            { data: 'ministry.name', name: 'ministry.name',searchable: false ,orderable: false},
		            {data: 'action', name: 'action',searchable: false,orderable: false}
		        ]
		    });
		});
	</script>
@endsection