<?php

Auth::routes();
Route::get('/', function () {
	if(Auth::User())
	{
		return view('home');
	} else {
		return view('auth.login');
	}
	
});
Route::get('get_quarter_list','Controller@getQuarterList');
Auth::routes();
Route::middleware(['auth'])->group(function () {
	Route::get('/home', 'HomeController@index')->name('home');
	
	Route::resource('/user', 'UserController');
	Route::get('datatable/user','UserController@datatable')->name('datatable.user');
	Route::resource('/rank','RankController');
	Route::resource('/ministry', 'MinistryController');
	Route::get('datatable/ministry','MinistryController@datatable')->name('datatable.ministry');
	
	Route::resource('/department', 'DepartmentController');
	Route::get('datatable/department','DepartmentController@datatable')->name('datatable.department');

	Route::resource('quarter', 'QuarterController');	
	Route::get('datatable/quarter', 'QuarterController@datatable')->name('datatable.quarter');	

	Route::resource('/housing_category', 'HousingCategoryController');
	Route::get('datatable/housing_category', 'HousingCategoryController@datatable')->name('datatable.housing_category');

	Route::resource('/position', 'PositionController');
	Route::get('datatable/position', 'PositionController@datatable')->name('datatable.position');

	Route::resource('/staff', 'StaffController');
	Route::get('datatable/staff', 'StaffController@datatable')->name('datatable.staff');

	Route::group(['prefix' => 'married','namespace'=>'Married'], function () {
		Route::resource('buildings','BuildingController');
		Route::get('datatable/buildings', 'BuildingController@datatable')->name('datatable.buildings');

		// end housing one

		Route::resource('allocate/','AllocateController');
		Route::get('{building_id}/allocate/init','AllocateController@init');
		Route::post('allocate/create','AllocateController@create');
		Route::get('allocate/{id}/edit','AllocateController@edit');
		Route::put('allocate/{id}','AllocateController@update');
		Route::get('{id}/allocate/remark_create','AllocateController@remark_create');
		Route::get('allocate/{id}/reallocate','AllocateController@reAllocate');

		Route::resource('allocated-ministries','AllocatedMinistryController');
		Route::get('datatable/allocated-ministries', 'AllocatedMinistryController@datatable')->name('datatable.allocated-ministries');

		Route::resource('allocated-quarters','AllocatedQuarterController');
		Route::get('datatable/allocated-quarters', 'AllocatedQuarterController@datatable')->name('datatable.allocated-quarters');

		Route::resource('allocated-buildings','AllocatedBuildingController');
		Route::get('datatable/allocated-buildings', 'AllocatedBuildingController@datatable')->name('datatable.allocated-buildings');

		Route::resource('allocated-total','AllocatedTotalController');

		Route::put('remove_staff/{id}','AllocatedBuildingController@removeStaff');

	});

});