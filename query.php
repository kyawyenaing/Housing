REATE VIEW totals AS 
SELECT ministry_id AS ministry , COUNT(*) AS total FROM allocates GROUP BY ministry_id;

<!--  -->
CREATE VIEW totals AS 
SELECT ministry_id AS ministry , COUNT(*) AS total FROM allocates GROUP BY ministry_id;

CREATE VIEW totals AS 
SELECT ministry_id AS ministry_building_id , COUNT(*) AS total FROM allocates GROUP BY ministry_id;

CREATE VIEW total_ministry AS SELECT * from allocates GROUP BY ministry_id,housing_category_id;

CREATE VIEW total_category AS SELECT * from total_ministry GROUP BY housing_category_id GROUP BY ministry_id;


CREATE VIEW total_results AS SELECT housing_category_id,quarter_id,ministry_id FROM allocates WHERE staff_id IS NOT NULL AND ministry_id IS NOT NULL GROUP BY ministry_id,quarter_id;


CREATE VIEW total_results AS SELECT COUNT(allocates.housing_category_id) AS count_category,COUNT(allocates.quarter_id) AS count_quarter,housing_category_id,quarter_id,ministry_id FROM allocates WHERE staff_id IS NOT NULL AND ministry_id IS NOT NULL GROUP BY ministry_id,housing_category_id,quarter_id;

CREATE VIEW result_filters AS SELECT * FROM total_results GROUP BY quarter_id,ministry_id;



CREATE VIEW total_results AS SELECT COUNT(allocates.housing_category_id) AS count_category,COUNT(allocates.quarter_id) AS count_quarter,housing_category_id,quarter_id,ministry_id FROM allocates WHERE staff_id IS NOT NULL AND ministry_id IS NOT NULL GROUP BY ministry_id,housing_category_id;


CREATE VIEW total_categories AS SELECT COUNT(allocates.housing_category_id) AS count_category,COUNT(allocates.quarter_id) AS count_quarter,housing_category_id,quarter_id,ministry_id FROM allocates WHERE staff_id IS NOT NULL AND ministry_id IS NOT NULL GROUP BY ministry_id,housing_category_id;



============================================
CREATE VIEW categories AS SELECT id FROM housing_categories WHERE id>3;


CREATE VIEW housing_quarters AS SELECT count_quarter,housing_category_id,quarter_id,ministry_id FROM total_results AS HousingQuarter WHERE housing_category_id IN (SELECT id FROM categories);

CREATE VIEW total_quarters AS SELECT count_quarter,housing_category_id,quarter_id,ministry_id,SUM(count_quarter) AS total_quarter FROM housing_quarters GROUP BY housing_category_id,ministry_id;



